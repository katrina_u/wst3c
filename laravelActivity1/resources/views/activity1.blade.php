<!DOCTYPE html>
<html>
<head>
<title>Activity 1</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<style>
  .float{
      position: relative;
      left: 150px;
      top: 20px;
      font-size: 20px;
      z-index: 2;
  }

  .border-clickme{
    border-width: medium;
    border-color: red;
  }
</style>
</head>
<body>
  <label class="float bg-white"> &nbsp;&nbsp;<b>Personal Information<b>&nbsp;&nbsp; </label>
  <div class="container">
    <div class="row">
      <div class="col mx-auto mt-2">
        <div class="card">
          <div class="card-body">
            <h6 class="card-subtitle mb-2">Firstname: </h6>
            <input type="text" placeholder="Enter your firstname">

            <h6 class="card-subtitle mb-2 mt-2">Lastname: </h6>
            <input type="text" placeholder="Enter your lastname">

            <h6 class="card-subtitle mb-2 mt-2">Username: </h6>
            <input type="text" placeholder="Enter your username">

            <h6 class="card-subtitle mb-2 mt-2">Password: </h6>
            <input type="password" class=" border-clickme" placeholder="Enter your password">

            <div class="form-floating mt-3">
              <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"></textarea>
            </div>


            <h6 class="card-subtitle mb-2 mt-2">Birthdate : <input type="date"> </h6>

            <h6 class="card-subtitle mb-2 mt-2 text-danger town" style="width: 16rem;">
              <label>Town: </label> 
             <select class="form-select" aria-label="Default select example">
              <option selected>Open this select menu</option>
              <option value="Anonas">Anonas</option>
              <option value="Bactad East">Bactad East</option>
              <option value="Bayaoas">Bayaoas</option>
              <option value="Cabaruan">Cabaruan</option>
              <option value="Cabuloan">Cabuloan</option>
              <option value="Camang">Camang</option>
              <option value="Camantiles">Camantiles</option>
              <option value="Nancayasan">Nancayasan</option>
             </select>
          </h6>

          <h6 class="card-subtitle mb-2 mt-2">Browser : <input type="text"> </h6>

          <button type="button" class="btn btn-success border-clickme">Click me</button>
          <button type="button" class="btn btn-outline-dark">Reset</button>
          <button type="button" class="btn btn-outline-dark">pindot me</button>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
