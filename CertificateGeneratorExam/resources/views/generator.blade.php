@extends('master')

@section('title')
    
  	<link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
  	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <link href="/css/A_home.css" rel="stylesheet" type="text/css">
	  <title>Generator</title>

    <style type="text/css">
      .hello{
        font-size: 30px;
      }
      .active::before{
        background: white;
      }
      .active{
        color: #D84315;
      }
    </style>
@endsection

@section('content')

  <header class="header" style="height:130px;">
    <img src="/assets/header.png" alt="" srcset="" class="img-fluid" style="width:400px;">
    <button class="header__btn_open-topnav header__btn"><span class="icon-menu-open"></span></button>
    <ul class="topnav topnav_mobile_show my-auto">
      <button class="header__btn_close-topnav header__btn"><span class="icon-menu-close"></span></button>
      <li class="topnav__item">
        <a href="seminars/" class="btn topnav__link">Seminars</a>
      </li>
      <li class="topnav__item">
        <a href="templates/" class="btn topnav__link">Templates</a>
      </li>
      <li class="topnav__item">
        <a href="generator/" class="btn topnav__link active">Generator</a>
      </li>
      <li class="topnav__item">
        <a href="certs/" class="btn topnav__link">Certificates</a>
      </li>
      <li class="topnav__item">
        <a href="/admins" class="btn topnav__link">Admins</a>
      </li>
      <li class="topnav__item">
        <div class="dropdown">
          <button class="btn topnav__link dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            My Profile
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <li><a class="dropdown-item" href="{{asset('manual/manual.pdf');}}" download>User's Manual</a></li>
            <li><a href="/logout" class="dropdown-item"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Logout</a></li>
          </ul>
        </div>
      </li>
    </ul>
    <div class="p-1 position-absolute bottom-0 end-0 text-end pe-3">
      <span id='ct6'></span>
    </div>
  </header>
 

  <div class="card mb-3 col-sm-6 mx-auto mt-5">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(Session::has('success-generator'))
      <div class="alert alert-success">
          {{ Session::get('success-generator') }}
          @php
              Session::forget('success-generator');
          @endphp
      </div>
    @endif
  
    <img class="card-img-top" src="assets/psu.png" alt="Card image cap">
    <div class="card-body">
      <div class = "container col-sm-12">
      <form action = "" method = "post" enctype="multipart/form-data">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <div class="form-row">
                <div class="input-group mb-3">
                  <span class="input-group-text text-dark fw-bold">Seminar</span>
                  <select class="form-select" aria-label="Default select example" name="seminar">
                    @foreach ($seminars as $seminar)
                    <option value="{{$seminar->id}}" <?php if(isset($_POST['seminar'])) if($_POST['seminar'] == $seminar->id) echo "selected";?>>{{$seminar->seminar_name}}</option>
                    @endforeach
                  </select>
                </div>  
                <div class="input-group mb-3">
                    <span class="input-group-text text-dark fw-bold">Awardee</span>
                    <input type="text" name="title" value ="<?php if (isset($_POST['preview'])) {echo $_POST['title'];}?>" class="form-control">
                </div>
                
                {{-- <div class="input-group mb-3">
                    <span class="input-group-text text-dark" style="background-color: #FFAB91;">URL Verification</span>
                    <input type="text" name="url" value ="<?php //if (isset($_POST['preview'])) {echo $_POST['url'];}?>" class="form-control">
                </div> --}}
  
                
                <center>
                  <button type="submit" name="preview" class="btn btn-lg text-white btn-block mx-auto" style="background-color: #082b54">Preview</button>
                </center>
            </div>
        </form>
    </div>
    </div>
  </div>


  <?php 
      if (isset($_POST['preview'])) {
        $seminar = $_POST['seminar'];
        $title = $_POST['title'];
        $title_len = strlen($_POST['title']);


        if ($title == "") {
          echo 
          "
          <div class='mx-auto alert alert-danger col-sm-6 text-center' role='alert'>
              Ensure you fill all the fields!
          </div>
          ";
        }else{
          echo 
          "
          <div class='mx-auto alert alert-success col-sm-6 text-center' role='alert' text-center>
              Here's the preview.
          </div>
          ";


          // Create connection
          $link=mysqli_connect("localhost","root", "", "certs_db") or die ("Error could not establish connection").mysqli_connect_error();

          $image = "assets/template.png";

          

          $selectQuery = "SELECT * FROM templates WHERE id = $seminar";
          $result = $link -> query($selectQuery);

          if(mysqli_num_rows($result) > 0){
            while($row=$result->fetch_array()){

                $bgtemp_id = $row[0];
                $bgtemp_name= $row[1];
                $imgpath= $row[2];

            }

          $createimage = imagecreatefrompng($imgpath);

          //this is going to be created once the generate button is clicked
          $output = "previews/"."newcertificate.png";


          // //then we make use of the imagecolorallocate inbuilt php function which i used to set color to the text we are displaying on the image in RGB format
          // $white = imagecolorallocate($createimage, 205, 245, 255);
          // $black = imagecolorallocate($createimage, 0, 0, 0);

          // //Then we make use of the angle since we will also make use of it when calling the imagettftext function below
          // $rotation = 0;

          // //we then set the x and y axis to fix the position of our text name
          // $origin_x = 200;
          // $origin_y=250;


          // //we then set the x and y axis to fix the position of our text occupation
          // $origin1_x = 185;
          // $origin1_y=300;

          // //we then set the differnet size range based on the length of the text which we have declared when we called values from the form
          // if($title_len<=7){
          //   $font_size = 25;
          //   $origin_x = 190;
          // }
          // elseif($title_len<=12){
          //   $font_size = 30;
          // }
          // elseif($title_len<=15){
          //   $font_size = 26;
          // }
          // elseif($title_len<=20){
          //   $font_size = 18;
          // }
          // elseif($title_len<=22){
          //   $font_size = 15;
          // }
          // elseif($title_len<=33){
          //   $font_size=11;
          // }
          // else {
          //   $font_size =10;
          // }

          // $certificate_text = $title;

          // //font directory for name
          // $drFont = "assets/Allura-Regular.TTF";

          // // font directory for occupation name
          // $drFont1 = "assets/Poppins-SemiBold.TTF";

          // //function to display name on certificate picture
          // $text1 = imagettftext($createimage, $font_size, $rotation, $origin_x, $origin_y, $black,$drFont, $certificate_text);

          // // $text3 = imagettftext($createimage, $font_size_content, $rotation, $origin1_x+2, $origin1_y, $black, $drFont1, $seminar);

          // $date = date('dH-i-s');

          // $code = md5($title.$date);

          // $QR=imagecreatefromstring(file_get_contents("https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=".$code."&choe=UTF-8"));
          // imagecopyresampled($createimage, $QR, 15, 352 , 0, 0, 110, 110, 100,100);

          // imagepng($createimage,$output,3);

          //then we make use of the imagecolorallocate inbuilt php function which i used to set color to the text we are displaying on the image in RGB format
          $white = imagecolorallocate($createimage, 205, 245, 255);
              $black = imagecolorallocate($createimage, 0, 0, 0);
    
              //Then we make use of the angle since we will also make use of it when calling the imagettftext function below
              $rotation = 0;
    
              //we then set the x and y axis to fix the position of our text name
              $origin_x = 215;
              $origin_y=225;
    
    
              // //we then set the x and y axis to fix the position of our text occupation
              // $origin1_x = 185;
              // $origin1_y=300;
    
              //we then set the differnet size range based on the length of the text which we have declared when we called values from the form
              if($title_len<=14){
                $font_size = 35;
                $origin_x = 230;
              }
              elseif($title_len<=13){
                $font_size = 30;
              }
              elseif($title_len<=15){
                $font_size = 26;
              }
              elseif($title_len<=20){
                $font_size = 18;
              }
              elseif($title_len<=22){
                $font_size = 15;
              }
              elseif($title_len<=33){
                $font_size=11;
              }
              else {
                $font_size =10;
              }
    
              $certificate_text = $title;
    
              //font directory for name
              $drFont = "assets/Allura-Regular.TTF";
    
              // font directory for occupation name
              $drFont1 = "assets/Poppins-SemiBold.TTF";
    
              //function to display name on certificate picture
              $text1 = imagettftext($createimage, $font_size, $rotation, $origin_x, $origin_y, $black,$drFont, $certificate_text);
    
              // $text3 = imagettftext($createimage, $font_size_content, $rotation, $origin1_x+2, $origin1_y, $black, $drFont1, $seminar);
    
              $date = date('dH-i-s');
    
              $code = md5($title.$date);
    
              $QR=imagecreatefromstring(file_get_contents("https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=".$code."&choe=UTF-8&chf=bg,s,FFFFFF10"));
              imagecopyresampled($createimage, $QR, 55, 310 , 0, 0, 110, 110, 100,100);
    
              imagepng($createimage,$output,3);



          



          

         
          

 ?>


        <!-- this displays the image below -->
        <div class = "container col-sm-6">
        	<img class="mx-auto d-block" src="<?php echo $output; ?>" >
        </div>
        
        <center>
          <div class="mt-4">
            <a href="generator/save/<?php echo $bgtemp_name."/".$title."/".$code; ?>" class="btn btn-success">Save Certificate</a>
          </div>
        </center>

        <!-- this provides a download button -->
        
        <br><br>
<?php 
        }else{
            echo "Error";
          }
        }
      }

     ?>

@include('sweetalert::alert')

@push('javascript-external')
@endpush

@stack('javascript-external')
@stack('javascript-internal')



@push('css-external')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2-bootstrap4.min.css') }}">
@endpush

@push('javascript-external')
<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
@endpush


@endsection


