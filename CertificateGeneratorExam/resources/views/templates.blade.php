@extends('master')

@section('title')

  <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

  <link href="/css/A_home.css" rel="stylesheet" type="text/css">

  {{-- Datatables --}}
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" ></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
  {{-- Datatables --}}

  <title>Templates</title>

  <style type="text/css">
    .hello{
      font-size: 30px;
    }
    .active::before{
      background: white;
    }
    .active{
      color: #D84315;
    }
  </style>
  @stack('css-external')
  @stack('css-internal')

@endsection

@section('content')

<header class="header" style="height:130px;">
  <img src="/assets/header.png" alt="" srcset="" class="img-fluid" style="width:400px;">
  <button class="header__btn_open-topnav header__btn"><span class="icon-menu-open"></span></button>
  <ul class="topnav topnav_mobile_show my-auto">
    <button class="header__btn_close-topnav header__btn"><span class="icon-menu-close"></span></button>
    <li class="topnav__item">
      <a href="seminars/" class="btn topnav__link">Seminars</a>
    </li>
    <li class="topnav__item">
      <a href="templates/" class="btn topnav__link active">Templates</a>
    </li>
    <li class="topnav__item">
      <a href="generator/" class="btn topnav__link">Generator</a>
    </li>
    <li class="topnav__item">
      <a href="certs/" class="btn topnav__link">Certificates</a>
    </li>
    <li class="topnav__item">
      <a href="/admins" class="btn topnav__link">Admins</a>
    </li>
    <li class="topnav__item">
      <div class="dropdown">
        <button class="btn topnav__link dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
          My Profile
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
          <li><a class="dropdown-item" href="{{asset('manual/manual.pdf');}}">User's Manual</a></li>
          <li><a href="/logout" class="dropdown-item"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Logout</a></li>
        </ul>
      </div>
    </li>
  </ul>
  <div class="p-1 position-absolute bottom-0 end-0 text-end pe-3">
    <span id='ct6'></span>
  </div>
</header>

  
  <div class="container card mb-3 col-sm-12 mx-auto mt-5">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(Session::has('success-template'))
      <div class="alert alert-success">
          {{ Session::get('success-template') }}
          @php
              Session::forget('success-template');
          @endphp
      </div>
    @endif

    
    <style>
      #templates{
        display: block;
        overflow-x: auto;
        white-space: nowrap; 
        height:200px;
      }
      #templates td{
        padding-left: 35px;
        padding-right: 35px;
      }
    </style>


     <!-- Modal -->
    <div class="modal fade" id="addImageModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title text-dark" id="exampleModalLabel">Add Personalized Template</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            <form action="/templates/addImage" method="post" enctype="multipart/form-data">
                @csrf
        
                <div class="row mb-3">
                  <div class="col-sm-12 mx-auto">
                      <label for="" class="form-label text-dark">Template<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" name="temp_name" required>
                  </div>
                  
                </div>
                <span aria-describedby="tempHelp"></span>

                <div class="row mb-3">
                    <div class="col-sm-12 mx-auto">
                        <label for="" class="form-label text-dark">Image<span class="text-danger">*</span></label>
                        <input type="file" class="form-control" name="temp_image" required>
                    </div>
                <div id="tempHelp" class="form-text">Background Template size 659 x 465 pixels</div>

                </div>


            
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Add</button>
            </div>

            </form>
        </div>
      </div>
    </div>

    <div class="card-body">
      <div class = "container col-sm-12">
      <form action = "" method = "post" enctype="multipart/form-data">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <div class="form-row">
      
                <div class="container mb-3">
                  <div class="row">
                    <table id="templates">
                      <tbody>
                        <tr>
                          <td>
                            <div class="text-center">
                              <button class="btn btn-success btn-lg mx-auto" data-bs-toggle="modal" data-bs-target="#addImageModal"><i class="fa fa-plus" aria-hidden="true"></i></button>
                            </div>
                            
                          </td>
                          @foreach ($bgtemplates as $bgtemplate)
                            
                            <td class="text-center">
                              <a target="_blank" href="{{$bgtemplate->imgpath}}">
                                <img src="{{asset($bgtemplate->imgpath)}}"  alt="" style="width: 200px;">
                              </a>
                            </td>
                        
                          @endforeach
                          
                        </tr>
                        <tr class="text-center fw-bold">
                          <td>
                            Add Another Template
                          </td>
                          @foreach ($bgtemplates as $bgtemplate)
                            
                            <td>
                              {{$bgtemplate->temp_name}}
                            </td>
                        
                          @endforeach
                          
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="input-group mb-3">
                  <span class="input-group-text text-dark fw-bold">Background Template</span>
                  <select class="form-select" aria-label="Default select example" name="template">
                    @foreach ($bgtemplates as $bgtemplate)
                    <option value="{{$bgtemplate->id}}" <?php if(isset($_POST['template'])) if($_POST['template'] == $bgtemplate->id) echo "selected";?>>{{$bgtemplate->temp_name}}</option>
                        
                    @endforeach
                  </select>
                </div>  

                <div class="input-group">
                
                  <span  class="input-group-text text-dark fw-bold">Seminar</span>
                  <select class="form-select" aria-label="#note" name="seminar">
                    @foreach ($seminars as $seminar)
                    <option value="{{$seminar->title}}" <?php if(isset($_POST['seminar'])) if($_POST['seminar'] == $seminar->title) echo "selected";?>>{{$seminar->title}}</option>
                    @endforeach
                  </select>
                  <span aria-describedby="emailHelp"></span>
                                
                </div>  
                <div id="emailHelp" class="form-text">Only one template per seminar</div>
                <div class="input-group my-3">
                    <span class="input-group-text text-dark fw-bold">Contents<span class="text-danger">*</span></span>
                    <textarea type="text" name="contents" value ="<?php if (isset($_POST['preview'])) {echo $_POST['contents'];}?>" class="form-control"><?php if (isset($_POST['preview'])) {echo $_POST['contents'];}?></textarea>
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text text-dark fw-bold">Logo</span>
                    <input type="file" name="logo" class="form-control">
                </div>

                <div class="input-group mb-3">
                  <span class="input-group-text text-dark fw-bold">Signatory Name 1<span class="text-danger">*</span></span>
                  <input type="text" name="signame1" class="form-control" value="<?php if (isset($_POST['signame1'])) {echo $_POST['signame1'];}?>">
                  <span class="input-group-text text-dark fw-bold">Position<span class="text-danger">*</span></span>
                  <input type="text" name="pos1" class="form-control"  value="<?php if (isset($_POST['pos1'])) {echo $_POST['pos1'];}?>">
                </div>
                <div class="input-group mb-3">
                  <span class="input-group-text text-dark fw-bold">E-signature 1<span class="text-danger">*</span></span>
                  <input type="file" name="esig1" class="form-control" value="<?php if (isset($_POST['esig1'])) {echo $_POST['esig1'];}?>" required>
                </div>
                <div class="input-group mb-3">
                  <span class="input-group-text text-dark fw-bold">Signatory Name 2</span>
                  <input type="text" name="signame2" class="form-control" value="<?php if (isset($_POST['signame2'])) {echo $_POST['signame2'];}?>">
                  <span class="input-group-text text-dark fw-bold">Position</span>
                  <input type="text" name="pos2" class="form-control" value="<?php if (isset($_POST['pos2'])) {echo $_POST['pos2'];}?>">
                </div>
                <div class="input-group mb-3">
                  <span class="input-group-text text-dark fw-bold">E-signature 2</span>
                  <input type="file" name="esig2" class="form-control" value="<?php if (isset($_POST['esig2'])) {echo $_POST['esig2'];}?>">
                </div>
                
                <center>
                  <button type="submit" name="preview" class="btn btn-lg btn-block text-white mx-auto" style="background-color: #082b54">Preview</button>
                </center>
            </div>
        </form>
    </div>
    </div>
  </div>


  <?php 
        if (isset($_POST['preview'])) {

          $seminar = $_POST['seminar'];
          $seminar_len = strlen($_POST['seminar']);
          // $title = $_POST['title'];
          // $title_len = strlen($_POST['title']);
          $contents= $_POST['contents'];
          // $url = $_POST['url'];
          $signame1 = $_POST['signame1'];
          $signame2 = $_POST['signame2'];
          $pos1 = $_POST['pos1'];
          $pos2 = $_POST['pos2'];
          $image_esig1 = $_FILES['esig1']['name'];
  
          $image_type = $_FILES['esig1']['type'];
          $image_size = $_FILES['esig1']['size'];
          $image_tmp_name= $_FILES['esig1']['tmp_name'];
          move_uploaded_file($image_tmp_name,"signatures/"."$image_esig1");
  
  
  
          $image_esig2 = $_FILES['esig2']['name'];
          $image_type = $_FILES['esig2']['type'];
          $image_size = $_FILES['esig2']['size'];
          $image_tmp_name= $_FILES['esig2']['tmp_name'];
          move_uploaded_file($image_tmp_name,"signatures/"."$image_esig2");
  
  
          if($_FILES['logo']['name'] != ""){
            $image_name = $_FILES['logo']['name'];
            $image_type = $_FILES['logo']['type'];
            $image_size = $_FILES['logo']['size'];
            $image_tmp_name= $_FILES['logo']['tmp_name'];
            move_uploaded_file($image_tmp_name,"logos/"."$image_name");
          }
          
  
          if ($contents) {
            $font_size_content = 10;
          }
  
          if ($contents == "") {
            echo 
            "
            <div class='mx-auto alert alert-danger col-sm-6 text-center' role='alert'>
                Ensure you fill all the fields!
            </div>
            ";
          }else{
            echo 
            "
            <div class='mx-auto alert alert-success col-sm-6 text-center' role='alert' text-center>
                Here's the preview.
            </div>
            ";
  
             // Create connection
            $link=mysqli_connect("localhost","root", "", "certs_db") or die ("Error could not establish connection").mysqli_connect_error();

            $image = "assets/template.png";
            $temp = $_POST['template'];
            $selectQuery = "SELECT * FROM bg_templates WHERE id = $temp";
            $result = $link -> query($selectQuery);

            if(mysqli_num_rows($result) > 0){

              while($row=$result->fetch_array()){

                $bgtemp_id = $row[0];
                $bgtemp_name = $row[1];
                $bgtemp_imgpath = $row[2];


              }


              //designed certificate picture
              $image = $bgtemp_imgpath;
    
              $createimage = imagecreatefrompng($image);
    
              //this is going to be created once the generate button is clicked
              $output = "previews/"."newpreview.png";
    
              //then we make use of the imagecolorallocate inbuilt php function which i used to set color to the text we are displaying on the image in RGB format
              $white = imagecolorallocate($createimage, 205, 245, 255);
              $black = imagecolorallocate($createimage, 0, 0, 0);
    
              //Then we make use of the angle since we will also make use of it when calling the imagettftext function below
              $rotation = 0;
    
              //we then set the x and y axis to fix the position of our text name
              $origin_x = 200;
              $origin_y=250;
    
    
              //we then set the x and y axis to fix the position of our text occupation
              $origin1_x = 185;
              $origin1_y=300;
    
              $sem_x = 160;
              $sem_y = 80;
    
              if($seminar_len<=50){
                $sem_x = 210;
              }
    
              if($seminar_len<=10){
                $sem_x = 320;
              }
    
              //$certificate_text = $title;
    
              //font directory for name
              $drFont = "assets/Allura-Regular.TTF";
    
              // font directory for occupation name
              $drFont1 = "assets/Poppins-SemiBold.TTF";
    
              // font directory for positions
              $posFont = "assets/Nunito-VariableFont_wght.TTF";
    
              //function to display name on certificate picture
              //$text1 = imagettftext($createimage, $font_size, $rotation, $origin_x, $origin_y, $black,$drFont, $certificate_text);
    
              //function to display occupation name on certificate picture
              $text2 = imagettftext($createimage, $font_size_content, $rotation, 120, 265, $black, $drFont1, $contents);
    
              $text3 = imagettftext($createimage, $font_size_content, $rotation, $sem_x, $sem_y, $black, $drFont1, $seminar);

              if($signame2 == "" || $pos2 == "" || $image_esig2 == ""){
                $text4 = imagettftext($createimage, $font_size_content, $rotation, 285, 370, $black, $drFont1, $signame1);
                $text6 = imagettftext($createimage, 9, $rotation, 305, 385, $black, $posFont, $pos1);
                $esig1=imagecreatefrompng("signatures/"."$image_esig1");
                imagecopy($createimage,$esig1, 280, 320, 0, 0, 100, 50);
              }else{
              
    
              $text4 = imagettftext($createimage, $font_size_content, $rotation, 180, 370, $black, $drFont1, $signame1);
              
              $text5 = imagettftext($createimage, $font_size_content, $rotation, 400, 370, $black, $drFont1, $signame2);
    
              $text6 = imagettftext($createimage, 9, $rotation, 200, 385, $black, $posFont, $pos1);
    
              $text7 = imagettftext($createimage, 9, $rotation, 420, 385, $black, $posFont, $pos2);

              
    
    
    
    
              $esig1=imagecreatefrompng("signatures/"."$image_esig1");
              imagecopy($createimage,$esig1, 175, 320, 0, 0, 100, 50);
    
    
    
              $esig2=imagecreatefrompng("signatures/"."$image_esig2");
              imagecopy($createimage,$esig2, 400, 320, 0, 0, 100, 50);
    
              // $text3 = imagettftext($createimage, $font_size_content, $rotation, $origin1_x+2, $origin1_y, $black, $drFont1, $seminar);

    
              // $QR=imagecreatefromstring(file_get_contents("https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl=".$url."&choe=UTF-8"));
                  // imagecopyresampled($createimage, $QR, 15, 352 , 0, 0, 110, 110, 100,100);
              }

              if($_FILES['logo']['name'] !=""){
                $str=imagecreatefrompng("logos/"."$image_name");
                imagecopy($createimage,$str, 60, 30 , 0, 0, 100, 100);
              }
             
              
    
              imagepng($createimage,$output,3);
            

            

  ?>


          <!-- this displays the image below -->
          <div class = "container col-sm-6">
            <img class="mx-auto d-block" src="<?php echo $output; ?>" >
          </div>
          
          <center>
            <div class="mt-4">
              <a href="templates/save/<?php echo $seminar."/".$bgtemp_id; ?>" class="btn btn-success">Add Design</a>
            </div>
          </center>

          <!-- this provides a download button -->
          
          <br><br>
  <?php 
          }
        }
        }

      ?>

  <script>
    $(function(){
          $("#templatesTable1").DataTable();
      });
      $(function(){
          $("#templatesTable2").DataTable();
      });
  </script>


  @push('javascript-external')
  <script src="{{asset('vendor/tinymce5/jquery.tinymce.min.js')}}"></script>
  <script src="{{asset('vendor/tinymce5/tinymce.min.js')}}"></script>
  <script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
  @endpush

  @push('javascript-internal')
  <script>
        $(document).ready(function() {
          //Event: delete
          $("form[role='alert']").submit(function(event) {
              event.preventDefault();
              Swal.fire({
              title: $(this).attr('alert-title'),
              text:  $(this).attr('alert-text'),
              icon: 'warning',
              allowOutsideClick: false,
              showCancelButton: true,
              cancelButtonText: $(this).attr('alert-btn-cancel'),
              reverseButtons: true,
              confirmButtonText: $(this).attr('alert-btn-yes'),
          }).then((result) => {
              if (result.isConfirmed) {
                //process ng deleting 
                event.target.submit();
              
              }
          });

          });
          //Texteditor content
        $("#input_template_content").tinymce({
        relative_urls: false,
        language: "en",
        plugins: [
          "advlist autolink lists link image charmap print preview hr anchor pagebreak",
          "searchreplace wordcount visualblocks visualchars code fullscreen",
          "insertdatetime media nonbreaking save table directionality",
          "emoticons template paste textpattern",
        ],
        toolbar1: "fullscreen preview",
        toolbar2:
          "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
          });

        });
        
    </script>
  @endpush

  <div class="container">
    <div class="row my-5">
      <div class="col-sm-12 mx-auto">
        <table class="table w-100 mx-auto" id="templatesTable2">
          <thead class="bg-dark text-white">
            <tr>
              <th>ID</th>
              <th>Seminar Title</th>
              <th>Template</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($templates as $template)
            <tr>
              <td>{{$template->id}}</td>
              <td>
                {{$template->seminar_name}}
                
              </td>
              <td>
                <a target="_blank" href="{{$template->img_path}}"><img class="img-fluid w-50" src="{{$template->img_path}}" alt="{{$template->seminar_name}}"></a>
                
              </td>
              <td>
                <form action="/templates/delete/{{$template->id}}" role="alert" method="GET" 
                  alert-title="{{trans('template.alert.delete.title')}}" alert-text="{{trans('template.alert.delete.message.confirm',['title' => $template->title])}}"
                  alert-btn-cancel="{{trans('template.button.cancel.value')}}" alert-btn-yes="{{trans('template.button.delete.value')}}" >
                <div class="text-center">
                <button type="submit" class="btn btn-sm btn-danger"> <i class=" fas fa-trash"></i>
                    </button>
                </div>
              </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  @include('sweetalert::alert')

  @stack('javascript-external')
  @stack('javascript-internal')

  @push('css-external')
  <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/select2/css/select2-bootstrap4.min.css') }}">
@endpush

@endsection

