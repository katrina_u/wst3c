<?php

namespace App\Http\Controllers;

use App\Mail\SendCertificate;
use App\Models\Certificate;
use App\Models\Email;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class MailController extends Controller
{
    //

    public function displayMailsent(Request $request, $id){
        // $emails = Email::orderBy('id', 'DESC')->get();

        $emails = Email::where([
            'cert_id' => $id
        ])->orderBy('id', 'DESC')->get();


        if(count($emails)>0){
            if ($request->session()->has('username')) {
                return view('mailsent', ['emails' => $emails]);
            }else{
                return redirect('admin');
            }
        }else{
            return redirect('certs');
        }

        
    }
    public function sendEmail(Request $request, $id){

        $request->validate([
            'email' => 'required|email',
        ]);

        $certs = DB::table('certificates')
                ->join('seminars', 'certificates.seminar_id', '=', 'seminars.id')
                ->select('certificates.id', 'seminars.title', 'seminars.sdate', 'seminars.edate', 'seminars.venue', 'certificates.id', 'certificates.awardee', 'certificates.img_path', 'certificates.token', 'certificates.creator', 'certificates.created_at')
                ->where('certificates.id', '=', $id)
                ->first();


        $email = $request->input('email');

        $insert = [
            "email" => $email,
            'cert_id' => $certs->id
        ];


        $details = [
            'email' => $email,
            'seminar' => $certs->title,
            'sdate' => $certs->sdate,
            'edate' => $certs->edate,
            'venue' => $certs->venue,
            'awardee' => $certs->awardee,
            'token' => $certs->token,
            'image' => public_path($certs->img_path),
        ];


        Email::create($insert);

        Mail::to($email)->send(new SendCertificate($details));

        Alert::success('Email Success', 'Email sent successfully');
        return redirect('certs');
    }
}
