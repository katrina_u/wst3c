<?php
/*
language : English
*/
return [
    'title' => [
        'index' => 'Categories',
        'create' => 'Add category',
        'edit' => 'Edit category',
        'detail' => 'Detail category',
    ],
'label' => [
	'no_data' => [
		'fetch' => "No category data yet",
		'search' => ":keyword category not found",
	]
    ],
    'form_control' => [
        'input' => [
            'title' => [
                'label' => 'Title',
                'placeholder' => 'Enter title',
                'attribute' => 'title'
            ],
            'slug' => [
                'label' => 'Slug',
                'placeholder' => 'Auto generate',
                'attribute' => 'slug'
            ],
            'thumbnail' => [
                'label' => 'Thumbnail',
                'placeholder' => 'Browse thumbnails',
                'attribute' => 'thumbnail'
            ],
            'search' => [
                'label' => 'Search',
                'placeholder' => 'Search for categories',
                'attribute' => 'search'
            ]
        ],
        'select' => [
            'parent_category' => [
                'label' => 'Parent category',
                'placeholder' => 'Choose parent category',
                'attribute' => 'parent category'
            ]
        ],
        'textarea' => [
            'description' => [
                'label' => 'Description',
                'placeholder' => 'Enter description',
                'attribute' => 'description'
            ],
        ]
    ],
    'button' => [
        'create' => [
            'value' => 'Add'
        ],
        'save' => [
            'value' => 'Save'
        ],
        'edit' => [
            'value' => 'Edit'
        ],
        'delete' => [
            'value' => 'Delete'
        ],
        'cancel' => [
            'value' => 'Cancel'
        ],
        'browse' => [
            'value' => 'Browse'
        ],
        'back' => [
            'value' => 'Back'
        ],
    ],
    'alert' => [
        'create' => [
            'title' => 'Add Seminar',
            'message' => [
                'success' => "Seminar saved successfully.",
                'error' => "An error occurred while saving the Seminar. :error"
            ]
        ],
        'update' => [
            'title' => 'Edit Seminar',
            'message' => [
                'success' => "Seminar updated successfully.",
                'error' => "An error occurred while updating the Seminar. :error"
            ]
        ],
        'delete' => [
            'title' => 'Delete Seminar',
            'message' => [
                'confirm' => "Are you sure you want to delete the :title Seminar?",
                'success' => "Seminar deleted successfully.",
                'error' => "An error occurred while deleting the Seminar. :error"
            ]
        ],
    ]
];
