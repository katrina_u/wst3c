<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Session;
use DB;

class ProductInsertController extends Controller
{
    public function insertform_prod(){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }
        return view('admin-prod', compact('data'));
    }

    public function insert_prod(Request $request) 
    {
        $pname = $request->input('pname');
        $pprice = $request->input('pprice');
        $pstocks = $request->input('pstocks');
        $pdate = $request->input('pdate');
        
        $res = DB::insert('insert into product (pname, pprice, pstocks, pdate) values(?,?,?,?)',[$pname, $pprice, $pstocks, $pdate]);
         if($res){
            return back()->with('success', 'Details was succesfully added!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
        
    }
}
