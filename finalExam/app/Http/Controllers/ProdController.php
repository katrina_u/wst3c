<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Session;
use DB;

class ProdController extends Controller
{
    public function index_prod()
    {
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }        
        $users = DB::select('select * from product');
        return view('admin-prod-dt',['users'=>$users], compact('data'));
    }


     public function showEditProd($id){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        } 
        $users = DB::select('select * from branches where id = ?',[$id]);
        return view('admin-prod-edit',['users'=>$users], compact('data'));
    }

    public function editProd(Request $request,$id){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }    

        $pname = $request->input('pname');
        $pprice = $request->input('pprice');
        $pstocks = $request->input('pstocks');
        $pdate = $request->input('pdate');
        
        $res = DB::update('update product set pname = ?, pprice = ?, pstocks = ?, pdate = ? where id = ?',[$pname, $pprice, $pstocks, $pdate, $id]);
            if($res){
                return back()->with('success', 'Details was succesfully updated!');
            } else{
                return back()->with('fail', 'Something went wrong');
            }
        $users = DB::select('select * from product where id = ?',[$id]);
        return view('admin-prod-edit',['users'=>$users], compact('data'));
    }

}
