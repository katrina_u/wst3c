<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Session;
use DB;

class TransactController extends Controller
{
    public function index_trans()
    {
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }        
        $users = DB::select('select * from transaction');
        return view('admin-trans',['users'=>$users], compact('data'));
    }
}
