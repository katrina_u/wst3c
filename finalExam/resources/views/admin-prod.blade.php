<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">

    <!-- Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500;600;700&family=Quattrocento:wght@400;700&display=swap" rel="stylesheet">


    <!-- Nav Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&family=Rubik&display=swap" rel="stylesheet">

    <title>Admin</title>



    <style>
      .bg-1 { 
        background-color: #c9686a;
      }

      body{
        background-color: #FFDCDC;
      }

      .h-cust-font {
          font-family: 'Oswald', sans-serif;
      }
      .f-col{
        color: #FFDCDC;
      }

      .cust-font {
          font-family: 'Quattrocento', serif;
      }

      .gen-font{
          font-family: 'Rubik', sans-serif;
      }

      .bg-pic {
         background: radial-gradient(rgba(
              0, 0, 0, 0.25), rgba(0, 0, 0, 0.50)), url('/images/flavor_4.jpg');
        background-size: cover;
        background-position: center;
        height: 60vh;
      }
      .truncate {
        max-width:500px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    </style>

   

  </head>
  <body>
    <!-- NavBar -->

<nav class="navbar navbar-expand-sm bg-1 justify-content-center">
    <a class="navbar-brand text-white" href="#">Hello, {{ $data->fname }}</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link text-dark" href="/ad-trans">Transaction</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="/ad-prod">Products</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link text-dark" href="logout">Log out</a>
      </li>
    </ul>
</nav>
    <!-- NavBar -->

    <!-- Body -->
    <div class="container-fluid">
      <div class="row bg-1 mt-2" style="margin-bottom: 0px;">
        <div class="col-sm-4">
          
        </div>

        <div class="col-sm-4">
           <div class="row">
               <p class="gen-font pt-3 text-center" style="font-size: 5vh; font-weight: bold; margin: 0px;"> ADD</p>
               <h6 class="gen-font pb-3 text-center f-col">Products</h6>
            </div>
        </div>

        <div class="col-sm-4 text-end">
          <div class="d-grid gap-2 d-md-block mt-3 mx-5 pt-3">
            <a href="/view-ad-prod" class="text-white text-decoration-none">
              <div class="d-grid gap-2 d-md-flex justify-content-md-end mb-4">
                <button class="btn btn-dark border-cust rounded-pill" type="button"><i class="fas fa-list-alt"></i> Records</button>
              </div>
            </a>
          </div>
        </div>
       
      </div>
    </div>
     <div class="container-fluid px-5 pt-3">
      <div class="row px-5">
        <div class="col">

          
              <!-- alert -->
              @if(Session::has('success'))
              <div class="alert alert-success alert-dismissible fade show border border-dark" role="alert">
                {{ Session::get('success') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif

              @if(Session::has('fail'))
              <div class="alert alert-secondary alert-dismissible fade show border border-dark" role="alert">
                {{ Session::get('fail') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif

           
          <form action="/create_products" class="border border-dark p-4 rounded" method="post" enctype="multipart/form-data" style="background-color: white;">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>"> 

            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingInput" placeholder="name@example.com" name="pname" required>
              <label for="floatingInput">Product Name</label>
            </div>

            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingPassword" placeholder="Password" name="pprice" required>
              <label for="floatingInput">Product Price</label>
            </div>

            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingPassword" placeholder="Password" name="pstocks" required>
              <label for="floatingInput">Product Stocks</label>
            </div>

            <div class="form-floating mb-3">
              <input type="date" class="form-control" id="floatingPassword" placeholder="Password" name="pdate" required>
              <label for="floatingInput">Product Date Created</label>
            </div>
            <div class="text-center w-70">
              <button type="submit" class="btn bg-1 mt-4 px-5 py-1 text-white">SAVE</button>    
            </div>
          </form>
        </div>        
      </div>
    </div>
    <!-- Body -->

    <!-- Footer -->
    <!-- Footer -->

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>


    <script>
      $(document).ready(function () {
          $('#example').DataTable();
      });
    </script> 
  </body>
</html>