<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <!-- Datatable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    
    <!-- Datatable Responsive -->
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.dataTables.min.css">

    <title>Admin</title>

    <style>
      .bg-1 { 
        background-color: #c9686a;
      }

      
      .f-col{
        color: #FFDCDC;
      }

      body{
        background-color: #FFDCDC;
      }

      .h-cust-font {
          font-family: 'Oswald', sans-serif;
      }

      .cust-font {
          font-family: 'Quattrocento', serif;
      }

      .gen-font{
          font-family: 'Rubik', sans-serif;
      }

      .bg-pic {
         background: radial-gradient(rgba(
              0, 0, 0, 0.25), rgba(0, 0, 0, 0.50)), url('/images/flavor_4.jpg');
        background-size: cover;
        background-position: center;
        height: 60vh;
      }
      .truncate {
        max-width:500px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    </style>

   

  </head>
  <body>
    <!-- NavBar -->

<nav class="navbar navbar-expand-sm bg-1 justify-content-center">
    <a class="navbar-brand text-white" href="#">Hello, {{ $data->fname }}</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link text-white" href="/ad-trans">Transaction</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="/ad-prods">Products</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link text-dark" href="logout">Log out</a>
      </li>
    </ul>
</nav>

    <!-- NavBar -->
 
    <!-- Body -->


     <div class="container-fluid p-5 ">
      <div class="row px-5">
        <div class="col">
          <div class="row p-5 bg-white rounded">
        <div class="col">
          <table id="example" class="display responsive nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>User ID</th>
                    <th>Product ID</th>
                    <th>Quantity</th>
                    <th>Date Purchased</th>
                </tr>
            </thead>
            <tbody>
              @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->user_id }}</td>
                    <td>{{ $user->product_id }}</td>
                    <td>{{ $user->quantity }}</td>
                    <td>{{ $user->date_purchased }}</td>
                </tr>           
              @endforeach
            </tbody>
        </table>
        </div>        
      </div>
        </div>        
      </div>
    </div>



    <!-- Body -->

    <!-- Footer -->

    <!-- Footer -->

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!--  Datatable JS -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>    
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script>
    <script>
    $(document).ready( function () {
      var table = $('#example').DataTable({
        columnDefs:[{targets:[0,1,2],className:"truncate"}],
        createdRow: function(row){
           $(row).find(".truncate").each(function(){
              $(this).attr("title", this.innerText);
           });
       }
      });

       $('#exampleModal').modal({ backdrop: 'static', keyboard: false });
    } );


   
    </script>
  </body>
</html>