<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\TransactController;
use App\Http\Controllers\ProductInsertController;
use App\Http\Controllers\ProdController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [CustomAuthController::class, 'login']);
Route::get('/reg', [CustomAuthController::class, 'registration']);
Route::post('/register-user', [CustomAuthController::class, 'registerUser'])->name('register-user');
Route::post('/login-user', [CustomAuthController::class, 'loginUser'])->name('login-user');
Route::get('/logout', [CustomAuthController::class, 'logout']);



//ADMIN
//transaction 
Route::get('/ad-trans', [TransactController::class, 'index_trans']);

//product
Route::get('/ad-prods', [ProductInsertController::class, 'insertform_prod']);
Route::post('/create_products', [ProductInsertController::class, 'insert_prod']); //insert post
Route::get('/view-ad-prod', [ProdController::class, 'index_prod']);
Route::get('admin-prod-edit/{id}', [ProdController::class, 'showEditProd']);
Route::post('admin-prod-edit/{id}', [ProdController::class, 'editProd']);
Route::get('admin-prod-delete/{id}', [ProdController::class, 'deleteProd']);