-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2022 at 11:26 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `etr_wst`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutus`
--

CREATE TABLE `aboutus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `aboutus_header` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aboutus_photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aboutus_desc` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `aboutus`
--

INSERT INTO `aboutus` (`id`, `aboutus_header`, `aboutus_photo`, `aboutus_desc`) VALUES
(1, 'products.jpg', 'aboutUsPhoto.jpg', 'Takoyaken Takoyaki is a takoyaki concept cart that will let you experience an authentic Japanese takoyaki here in the Philippines. Comes with different flavors that will suffice your cravings of a delicious yet healthy snacks for your tummy 1Test');

-- --------------------------------------------------------

--
-- Table structure for table `bev_menu`
--

CREATE TABLE `bev_menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bev_photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bev_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bev_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bev_desc` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bev_menu`
--

INSERT INTO `bev_menu` (`id`, `bev_photo`, `bev_name`, `bev_price`, `bev_desc`) VALUES
(13, '1655649200.jpg', 'Wintermelon', '60', 'Milk tea16 oz'),
(14, '1655482294.jpg', 'Okinawa', '65', 'Milktea 16 oz'),
(15, '1655482273.jpg', 'Brown Sugar', '65', 'Milktea 16 oz'),
(16, '1655482328.jpg', 'Taro', '65', 'Milktea 16 oz'),
(17, '1655649184.jpg', 'Lychee', '65', 'Milktea 65 oz');

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bg_photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b_reg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b_prov` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b_city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b_brgy` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b_postal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `bg_photo`, `b_reg`, `b_prov`, `b_city`, `b_brgy`, `b_postal`) VALUES
(8, '1655649272.jpg', '1', 'Pangasinan Test', 'Rosales City', 'Zone 2', '2441'),
(9, '1655417399.jpg', '1', 'Ilocos Sur', 'Vigan City', 'City of Vigan', '2702'),
(10, '1655417683.jpg', '3', 'Cavite', 'Nueno', 'Unit 9 Terminus Plaza', '4104'),
(11, '1655417535.jpg', '1', 'Isabela', 'Naguilian', 'Magsaysay', '3302'),
(12, '1655417635.jpg', '4', 'Laguna', 'San Pedro', 'No. 82 Macaria cor. P. Ocampo St. Ph 8, Pacita 1', '4023'),
(13, '1655649253.jpg', '1', 'Pangasinan', 'Manaoag', 'Poblacion', '1233');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `contype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `condel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`id`, `contype`, `condel`) VALUES
(1, 'Phone Number', '0995 834 6293'),
(13, 'Email', 'takoyakentakoyaki.ty@gmail.com'),
(14, 'Facebook Page', 'Takoyaken Takoyaki'),
(15, 'Twitter', '@takoyakenTakoyaki');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE `footer` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `desc` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`id`, `desc`) VALUES
(1, 'TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1TEST1');

-- --------------------------------------------------------

--
-- Table structure for table `homepage`
--

CREATE TABLE `homepage` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sBranch` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sPhrase` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sHeaderPhoto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sCommit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sStoryPhoto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sProPhoto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sProDesc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sLocation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sLocPhoto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homepage`
--

INSERT INTO `homepage` (`id`, `sBranch`, `sPhrase`, `sHeaderPhoto`, `sCommit`, `sStoryPhoto`, `sProPhoto`, `sProDesc`, `sLocation`, `sLocPhoto`) VALUES
(1, 'Urdaneta City Branch', 'Lasang Binabalikan, Takoyaking Pinipilahan', 'aboutUsPhoto.jpg', 'This delicious batter consists of a lot of different ingredients.', 'store1.jpg', 'products.jpg', 'We make our takoyaki shell with a traditional egg and flour, base but with a secret twist ~ - they add a secret base soup so that the resulting takoyaki will have a puffy shell but a creamy, soft inside!', '18 Relocation Site Urdaneta Central School Stalls, Alexander St., Urdaneta City', 'event1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `h_photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loc_photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brgy` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `h_photo`, `loc_photo`, `region`, `province`, `city`, `brgy`, `postal`) VALUES
(1, 'products.jpg', 'store1.jpg', '1', 'Pangasinan', 'Urdaneta City', '18 Relocation Site Urdaneta Central School Stalls, Alexander St.', '2428');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(35, '2014_10_12_100000_create_password_resets_table', 1),
(36, '2019_08_19_000000_create_failed_jobs_table', 1),
(37, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(38, '2022_05_17_084324_create_users_table', 1),
(50, '2022_06_07_053411_create_branches_table', 7),
(57, '2022_06_15_043038_create_footer_table', 11),
(58, '2022_06_15_085543_create_homepage_table', 12),
(59, '2022_06_15_105838_create_aboutus_table', 13),
(60, '2022_06_15_144611_create_contactus_table', 14),
(61, '2022_06_16_032340_create_location_table', 15),
(62, '2022_06_16_103834_create_takoyaki_menu_table', 16),
(63, '2022_06_16_110221_create_bev_menu_table', 17),
(68, '2022_06_19_073755_create_user_orders_table', 18),
(69, '2022_06_19_080704_create_user_order_bev_table', 19);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `takoyaki_menu`
--

CREATE TABLE `takoyaki_menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tak_photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tak_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tak_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tak_desc` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `takoyaki_menu`
--

INSERT INTO `takoyaki_menu` (`id`, `tak_photo`, `tak_name`, `tak_price`, `tak_desc`) VALUES
(15, '1655649141.jpg', 'Cheese 1', '40', 'This is a 4 piece takoyaki stuffed with cheese inside!'),
(16, '1655481349.jpg', 'Whole Baby Octopus', '100', 'This is a 3 piece takoyaki stuffed with a whole baby octopus inside!'),
(17, '1655481403.jpg', 'Chicken Melt Cheese', '55', 'This is a 4 piece takoyaki stuffed with a chicken melt cheese inside!'),
(18, '1655481440.jpg', 'Crab', '55', 'This is a 4 piece takoyaki stuffed with a small piece of crab inside!'),
(19, '1655481472.jpg', 'Bacon', '45', 'This is a 4 piece takoyaki stuffed with a bacon inside!'),
(20, '1655481513.jpg', 'Octobits', '55', 'This is a 4 piece takoyaki stuffed with a octobits inside!'),
(21, '1655649107.jpg', 'Baby Octobits', '45', '4 pieces stuffed with baby octobits!');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `password`) VALUES
(1, 'Katrina', 'Urbano', 'katrinau@gmail.com', '$2y$10$93g2BkpKTuXZG36uDM0C3OCpY0UBkQGVE5xmtKfMoh/8iwwq5zhAm'),
(2, 'Katrina', 'Urbano', 'admin@gmail.com', '$2y$10$ZcecZjnb07nkRQJT5WHZAu8s/4RAjgkh8qju3ap/JRhVkYNPgsTme');

-- --------------------------------------------------------

--
-- Table structure for table `user_orders`
--

CREATE TABLE `user_orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_orders`
--

INSERT INTO `user_orders` (`id`, `user_order`, `user_quantity`) VALUES
(1, 'Whole Baby Octopus', '2'),
(2, 'Chicken Melt Cheese', '2'),
(3, 'Cheese', '2'),
(26, 'Cheese', '3'),
(27, 'Cheese', '3'),
(28, 'Cheese', '4'),
(29, 'Cheese', '4'),
(30, 'Cheese', '2'),
(31, 'Cheese', '2'),
(32, 'Brown Sugar', '3'),
(33, 'Chicken Melt Cheese', '4'),
(34, 'Brown Sugar', '5');

-- --------------------------------------------------------

--
-- Table structure for table `user_order_bev`
--

CREATE TABLE `user_order_bev` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_order_bev` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_quantity_bev` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_order_bev`
--

INSERT INTO `user_order_bev` (`id`, `user_order_bev`, `user_quantity_bev`) VALUES
(1, 'Wintermelon', '2'),
(2, 'Wintermelon', '2'),
(3, 'Wintermelon', '2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aboutus`
--
ALTER TABLE `aboutus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bev_menu`
--
ALTER TABLE `bev_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homepage`
--
ALTER TABLE `homepage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `takoyaki_menu`
--
ALTER TABLE `takoyaki_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_orders`
--
ALTER TABLE `user_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_order_bev`
--
ALTER TABLE `user_order_bev`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aboutus`
--
ALTER TABLE `aboutus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bev_menu`
--
ALTER TABLE `bev_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `footer`
--
ALTER TABLE `footer`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `takoyaki_menu`
--
ALTER TABLE `takoyaki_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_orders`
--
ALTER TABLE `user_orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `user_order_bev`
--
ALTER TABLE `user_order_bev`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
