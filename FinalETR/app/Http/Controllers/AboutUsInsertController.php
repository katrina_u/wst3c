<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Session;
use DB;

class AboutUsInsertController extends Controller
{
    public function insertform_about(){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }
        return view('adminAboutUs.ADaboutus', compact('data'));
    }

    public function insert_about(Request $request) 
    {
        $aboutus_photo = $request->input('aboutus_photo');
        $aboutus_desc = $request->input('aboutus_desc');
        
        $res = DB::insert('insert into aboutus (aboutus_photo, aboutus_desc) values(?,?)',[$aboutus_photo, $aboutus_desc]);
         if($res){
            return back()->with('success', 'Details was succesfully added!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
        
    }
}
