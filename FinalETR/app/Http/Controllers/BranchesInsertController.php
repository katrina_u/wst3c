<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Session;
use DB;

class BranchesInsertController extends Controller
{
    public function insertform_branches(){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }
        return view('adminBranches.ADbranches', compact('data'));
    }

    public function insert_branches(Request $request) 
    {
        if($request->hasfile('bg_photo')){
            $file = $request->file('bg_photo');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('images', $filename);
        }
        $b_reg = $request->input('b_reg');
        $b_prov = $request->input('b_prov');
        $b_city = $request->input('b_city');
        $b_brgy = $request->input('b_brgy');
        $b_postal = $request->input('b_postal');
        $res = DB::insert('insert into branches (bg_photo, b_reg, b_prov, b_city, b_brgy, b_postal) values(?,?,?,?,?,?)',[$filename, $b_reg, $b_prov, $b_city, $b_brgy, $b_postal]);
        if($res){
            return back()->with('success', 'Branch was successfully added!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
    }
}
