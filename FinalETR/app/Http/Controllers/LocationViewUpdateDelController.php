<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Models\User;

class LocationViewUpdateDelController extends Controller
{
    public function index_AdminEditLocation(){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        } 
        $users = DB::select('select * from location');
        return view('adminLocation.EditLocation',['users'=>$users], compact('data'));
    }

    public function index_ClientLocation(){
        $users1 = DB::select('select * from location');
        return view('CLlocation',['users'=>$users1]);
    }

    public function showEditLocation($id){
        
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        } 

        $users = DB::select('select * from location where id = ?',[$id]);
        return view('adminLocation.eLocation',['users'=>$users], compact('data'));
    }

    public function editLocation(Request $request,$id){

        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        } 

        $h_photo = $request->input('h_photo');
        $loc_photo = $request->input('loc_photo');
        $region = $request->input('region');
        $province = $request->input('province');
        $city = $request->input('city');
        $brgy = $request->input('brgy');
        $postal = $request->input('postal');
        
        $res = DB::update('update location set h_photo=?, loc_photo=?, region=?, province=?, city=?, brgy=?, postal=? where id=?',[$h_photo, $loc_photo, $region, $province, $city, $brgy, $postal, $id]);

        if($res){
            return back()->with('success', 'Location details was succesfully updated!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }

        $users = DB::select('select * from location where id = ?', [$id]);
        return view('adminLocation.eLocation',['users'=>$users], compact('data'));
    }

}   
