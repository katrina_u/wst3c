<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Session;
use DB;

class LocationInsertController extends Controller
{
    public function insertform_loc(){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }
        return view('adminLocation.ADlocation', compact('data'));
    }

    public function insert_loc(Request $request) 
    {
        $loc_photo = $request->input('loc_photo');

        $region = $request->input('region');
        $province = $request->input('province');
        $city = $request->input('city');
        $brgy = $request->input('brgy');
        $postal = $request->input('postal');
        
        $res = DB::insert('insert into location (loc_photo, region, province, city, brgy, postal) values(?,?,?,?,?,?)',[$loc_photo, $region, $province, $city, $brgy, $postal]);
        if($res){
            return back()->with('success', 'Location was successfully added!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
    }
}
