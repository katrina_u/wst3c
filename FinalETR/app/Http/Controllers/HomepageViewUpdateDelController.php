<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Models\User;

class HomepageViewUpdateDelController extends Controller
{   
    public function index_AdminEditHomepage()
    {
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }        
        $users = DB::select('select * from homepage');
        return view('adminHomepage.EditHomepage',['users'=>$users], compact('data'));
    }

    public function index_ClientHomepage(){
        $users = DB::select('select * from homepage');
        return view('homepage',['users'=>$users]);

        $users1 = DB::select('select * from location');
        return view('homepage',['users'=>$users1]);        
    }

    public function showEditHomepage($id){
        
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        } 
        $users = DB::select('select * from homepage where id = ?',[$id]);
        return view('adminHomepage.eHomepage',['users'=>$users], compact('data'));
    }

    public function editHomepage(Request $request,$id){

        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        } 

        $sBranch = $request->input('sBranch');
        $sPhrase = $request->input('sPhrase');
        $sHeaderPhoto = $request->input('sHeaderPhoto');
        $sCommit = $request->input('sCommit');
        $sStoryPhoto = $request->input('sStoryPhoto');
        $sProPhoto = $request->input('sProPhoto');
        $sProDesc = $request->input('sProDesc');
        $sLocation = $request->input('sLocation');
        $sLocPhoto = $request->input('sLocPhoto');
        
        $res = DB::update('update homepage set sBranch=?, sPhrase=?, sHeaderPhoto=?, sCommit=?, sStoryPhoto=?, sProPhoto=?, sProDesc=?, sLocation=?, sLocPhoto=? where id=?',[$sBranch, $sPhrase, $sHeaderPhoto, $sCommit, $sStoryPhoto, $sProPhoto, $sProDesc, $sLocation, $sLocPhoto, $id]);

        if($res){
            return back()->with('success', 'Homepage details was succesfully updated!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }

        $users = DB::select('select * from homepage where id = ?', [$id]);
        return view('adminHomepage.eHomepage',['users'=>$users], compact('data'));
    }
    

}
