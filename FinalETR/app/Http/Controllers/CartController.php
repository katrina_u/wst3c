<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Models\User;

class CartController extends Controller
{
    public function index_cart(){
        $users = DB::select('select * from user_orders');
        return view('cart',['users'=>$users]);
    }

    public function index_admincart(){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        } 
        $users = DB::select('select * from user_orders');
        return view('admin-cart',['users'=>$users], compact('data'));
    }
}
