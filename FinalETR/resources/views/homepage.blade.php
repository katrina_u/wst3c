<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <!-- Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500;600;700&family=Quattrocento:wght@400;700&display=swap" rel="stylesheet">

    <!-- Nav Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&family=Rubik&display=swap" rel="stylesheet">


    <title>Homepage</title>

    <style>
      .bg-1 { 
        background-color: #FF9898;
      }

      @foreach ($users as $user)
      .bg-pic {
         background: radial-gradient(rgba(
              0, 0, 0, 0.90), rgba(0, 0, 0, 0.50)), url({{ url('images/'.$user->sHeaderPhoto) }});
        background-size: cover;
        background-position: center;
        height: 73vh;
      }
      @endforeach

      .h-cust-font {
        font-family: 'Oswald', sans-serif;
      }
      .cust-font {
        font-family: 'Quattrocento', serif;
      }

      body{
        background-color: #FFDCDC;
      }

      .header-img {
        object-fit: cover;
        opacity: 0.35;
      }

      .header-bg{
        background: black;
      }

      .bg-white{
        background-color: white;
      }

      .gen-font{
          font-family: 'Rubik', sans-serif;
      }
    </style>
  </head>
  <body>
    <!-- NavBar -->
   <div class="container-fluid sticky-top">
          <div class="row">
            <nav class="navbar navbar-expand-sm bg-1 sticky-top gen-font p-3">
              <div class="col-sm-5">
                <div class="row">
                  <div class="col-sm-2">
                    <a class="py-5 px-2" href="Chome"><img src="images/logo_noBG.png" style="width: 70px;" alt=""></a>
                  </div>
                  <div class="border-start col-sm-3">
                     <a class="ps-2 text-decoration-none " href="Chome">
                      <h4 class="pt-1 h-cust-font text-dark">Takoyaken Takoyaki</h4>
                      </a>
                  </div>
                </div>
              </div>

              <div class="col-sm-5">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <ul class="navbar-nav justify-content-center">
                  <li class="nav-item">
                    <a class="nav-link text-white" aria-current="page" href="Chome">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-dark" href="Cmenu">Menu</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-dark" href="Cbranches">Branches</a>
                  </li>
                  <li class="nav-item">
                  <a class="nav-link text-dark" href="Clocation">Location</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-dark" href="Ccontact">Contact Us</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link text-dark" href="Cabout">About Us</a>
                  </li> 
                  </ul>
              </div>
              <div class="col-sm-1">
                <a href="shopping-cart"><i class="fas fa-shopping-cart text-dark"></i></a>
              </div>
            </nav>
          </div>
        </div>
    <!-- NavBar -->

    <!-- Body -->
    @foreach ($users as $user)
    <div class="container-fluid">
      <div class="row">
        <div class="bg-pic pt-0 text-center text-white" style="padding-top: 30vh;">
            <p class="h-cust-font" style="margin-top: 20vh; font-size: 90px;">TAKOYAKEN TAKOYAKI</p> 
            <h4 class="cust-font">{{ $user->sBranch }}</h4> 
            <h5 class="cust-font" style="padding-bottom: 10vh;">"{{ $user->sPhrase }}"</h5>
        </div>
      </div>

      <div class="row text-center">
        <div class="col p-5 m-5">
          <h2 class="h-cust-font">OUR COMMITMENT</h2>
          <h6 class="cust-font pt-3">{{ $user->sCommit }}</h6>
        </div>
      </div>  


      <div class="row text-center">
        <div class="col-md-6 bg-white">
          <h1 class="h-cust-font" style="margin-top: 20vh;">TAKOYAKEN TAKOYAKI'S STORY</h1>
          <a href="Cabout" target="_blank"><button type="button" class="btn bg-1 mt-3" >READ MORE</button></a>
        </div>
        <div class="header-bg col-6 rounded mx-auto d-block p-0">
          <img src=" {{ url('images/'.$user->sStoryPhoto) }}" class="header-img img-fluid" alt="">
        </div>
      </div>  

      <div class="row text-center"> 
        <div class="header-bg col-6 rounded mx-auto d-block p-0">
          <img src="{{ url('images/'.$user->sProPhoto) }}" class="header-img img-fluid " alt="">
        </div>
        <div class="col-md-6">
          <h1 class="h-cust-font pb-2" style="margin-top: 18vh;">PRODUCTS</h1>
          <p class="px-5 cust-font">{{ $user->sProDesc }}</p>
        </div> 
      </div>

       <div class="row text-center">
        <div class="col-md-6 bg-white">
          <h1 class="h-cust-font" style="margin-top: 15vh;">STORE LOCATION</h1>
          <p class="cust-font px-5 mx-5">{{ $user->sLocation }}</p>
        </div>
        <div class="header-bg col-6 rounded mx-auto d-block p-0 gen-font">
          <img src="{{ url('images/'.$user->sLocPhoto) }}" class="header-img img-fluid " alt="">
        </div>
      </div>    

    <div class="row text-center p-3">
      <div class="col-md-3">
        </div>
          <div class="col-md-6 bg-white">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d246.75758715455686!2d120.56609185982133!3d15.975663019965653!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33913f5b17b1df13%3A0x1ec51c6893da298a!2sYza%E2%80%99s%20Tea%202Go%20Urdaneta-Central%20Branch!5e0!3m2!1sen!2sph!4v1655705030159!5m2!1sen!2sph" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
          </div> 
        <div class="col-md-3">
        </div>   
      </div>
    </div>
     @endforeach
    <!-- Body -->

    <!-- Footer -->
        <footer class=" bg-1 text-center text-white">
      <div class="container-fluid p-4" style="background-color: rgba(0, 0, 0, 0.2);">
        <div class="row">
          <div class="col-sm-6 p-3">
            <div class="text-center pt-3">
                © 2022 Copyright:
              <a class="text-white" href="https://www.facebook.com/takoyakenurdaneta" target="_blank">Takoyaken Takoyaki</a>
              <p>Developed by Katrina G. Urbano</p>
            </div>
          </div>
          <div class="col-sm-6 p-3">
            <a class="btn btn-outline-dark btn-floating ms-5 mt-4" href="https://www.facebook.com/takoyakenurdaneta" role="button" target="_blank"><i class="fab fa-facebook-f" ></i> Facebook</a>

            <a class="btn btn-outline-dark btn-floating ms-5 mt-4" href="#" role="button"><i class="fab fa-instagram"></i> Instagram</a>

            <a class="btn btn-outline-dark btn-floating ms-5 mt-4" href="#" role="button"><i class="fab fa-twitter"></i> Twitter</a>
          </div>
        </div>
      </div>     
    </footer>
    <!-- Footer -->

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>