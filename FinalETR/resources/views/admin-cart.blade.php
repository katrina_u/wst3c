<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

     <!-- Datatable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    
    <!-- Datatable Responsive -->
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.dataTables.min.css">

    <title>Contact Us DT - Admin</title>

    <style>
      .bg-1 { 
        background-color: #c9686a;
      }


      .f-col{
        color: #FFDCDC;
      }

      body{
        background-color: #FFDCDC;
      }

      .h-cust-font {
          font-family: 'Oswald', sans-serif;
      }

      .cust-font {
          font-family: 'Quattrocento', serif;
      }

      .gen-font{
          font-family: 'Rubik', sans-serif;
      }

      .bg-pic {
         background: radial-gradient(rgba(
              0, 0, 0, 0.25), rgba(0, 0, 0, 0.50)), url('/images/flavor_4.jpg');
        background-size: cover;
        background-position: center;
        height: 60vh;
      }
      .truncate {
        max-width:500px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    </style>

   

  </head>
  <body>
    <!-- NavBar -->

<nav class="navbar navbar-expand-sm bg-1 justify-content-center">
    <a class="navbar-brand text-white" href="#">Hello, {{ $data->fname }}</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link text-dark" href="view-ADMINhomepage">Homepage</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINmenu">Menu</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="admin-shop-cart">Shopping Cart</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINbranches">Branches</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" aria-current="page" href="view-ADMINlocation">Location</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="view-ADMINabout">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINcontact">Contact Us</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link text-dark" href="logout">Log out</a>
      </li>
    </ul>
</nav>

    <!-- NavBar -->

    <!-- Body -->
    
      <div class="container-fluid ">
      <div class="row pt-2">
        <div class="col-sm-2"></div>
        <div class="col-sm-8 rounded bg-white mb-2 border border-dark">
          <table class="table">
            <thead>
              <tr>
                <th class="text-center" scope="col">Product</th>
                <th class="text-center" scope="col">Quantity</th>
                <th class="text-center" scope="col">Action</th>
              </tr>
            </thead>
            <tbody>  
              @foreach ($users as $user)
              <tr>
                <td>
                  <div class="form-check">
                    <label class="form-check-label" for="defaultCheck1">
                      {{ $user->user_order }}
                    </label>
                  </div>
                </td>   
                <td class="text-center">{{ $user->user_quantity }}</td>
                <td class="text-center">
                  <div class="">
                    <button class="btn btn-success me-md-2" type="button">Deliver</button>
                    <button class="btn btn-danger" type="button">Cancel</button>
                  </div>
                </td>
              </tr> 
              @endforeach
            </tbody>
          </table>
        </div>        
        <div class="col-sm-2"></div>
      </div>
    </div>
    <!-- Body -->

    <!-- Footer -->
    <footer class="bg-1 text-center text-white">
      <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2); margin-top: 12%;">
        © 2022 Copyright:
        <a class="text-white" href="https://mdbootstrap.com/">Katrina G. Urbano</a>
      </div>
    </footer>
    <!-- Footer -->

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!--  Datatable JS -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>    
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.3.0/js/
    dataTables.responsive.min.js"></script>

    <script>
    $(document).ready(function() {
    $('#example').DataTable({
        responsive:true
      })
    
       $('#exampleModal').modal({ backdrop: 'static', keyboard: false });
    } );
    </script>

  </body>
</html>