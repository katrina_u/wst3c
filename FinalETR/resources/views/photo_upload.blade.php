<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">

    <title>Homepage</title>

    <style>
      .bg-1 { 
        background-color: #FF9898;
      }

      .bg-pic {
         background: radial-gradient(rgba(
              0, 0, 0, 0.25), rgba(0, 0, 0, 0.50)), url('/images/flavor_4.jpg');
        background-size: cover;
        background-position: center;
        height: 60vh;
      }

      .body-col{
        background-color: #FFDCDC;
      }
    </style>

   

  </head>
  <body>
    <!-- NavBar -->

<nav class="navbar navbar-expand-sm bg-1 justify-content-center">
    <a class="navbar-brand text-white" href="#">Hello, {{ $data->fname }}</a>
    <a class="navbar-brand text-white" href="ADMINbranches">Branches</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINmenu">Menu</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" aria-current="page" href="ADMINloc">Location</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINhome">Homepage</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINabout">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINcontact">Contact Us</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link text-dark" href="logout">Log out</a>
      </li>
    </ul>
</nav>
    <ul class="nav justify-content-center body-col p-2">
      <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="#Add">ADD</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#Edit">EDIT</a>
      </li>
    </ul>
    <!-- NavBar -->

    <!-- Body -->
     <div class="container-fluid p-5">
      <div class="row px-5">
        <div class="col">
          <form method="post" action="/upload" class="border border-dark p-4 rounded"  enctype="multipart/form-data">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <div class="form-floating mb-3">
              <input type="file" class="form-control" id="floatingPassword" placeholder="Password" name="branches_photo">
              <label for="floatingInput">Photo</label>
            </div>
            <button type="submit" class="btn bg-1 mt-4">Save</button>
          </form>

        <table id="example" class="table table-striped text-center mt-5" style="width:100%">
          <thead>
              <tr>
                  <th>Photo</th>
                  <th>Region</th>
                  <th>Province</th>
                  <th>City</th>
                  <th>Barangay</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>
            @foreach ($users as $user)
              <tr>
                  <td>{{ $user->branches_photo }}</td>
                  <td>System Architect</td>
                  <td>Edinburgh</td>
                  <td>61</td>
                  <td>2011-04-25</td>
                  <td>$320,800</td>
              </tr>
            @endforeach
          </tbody>
          <tfoot>
              <tr>
                  <th>Photo</th>
                  <th>Region</th>
                  <th>Province</th>
                  <th>City</th>
                  <th>Barangay</th>
                  <th>Action</th>
              </tr>
          </tfoot>
        </table>
        </div>        
      </div>
    </div>
    <!-- Body -->

    <!-- Footer -->
    <footer class="bg-1 text-center text-white">
      <div class="container p-4">
        <section class="mb-4">
          <!-- Facebook -->
          <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
            ><i class="fab fa-facebook-f"></i
          ></a>

          <!-- Instagram -->
          <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
            ><i class="fab fa-instagram"></i
          ></a>
        </section>
        <section class="mb-4">
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt distinctio earum
            repellat quaerat voluptatibus placeat nam, commodi optio pariatur est quia magnam
            eum harum corrupti dicta, aliquam sequi voluptate quas.
          </p>
        </section>
        <section class="">
          <div class="row">
            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
              <h5 class="text-uppercase">Links</h5>

              <ul class="list-unstyled mb-0">
                <li>
                  <a href="#!" class="text-white">Link 1</a>
                </li>
                <li>
                  <a href="#!" class="text-white">Link 2</a>
                </li>
                <li>
                  <a href="#!" class="text-white">Link 3</a>
                </li>
                <li>
                  <a href="#!" class="text-white">Link 4</a>
                </li>
              </ul>
            </div>
          </div>
        </section>
      </div>

      <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2020 Copyright:
        <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
      </div>
    </footer>
    <!-- Footer -->

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>


    <script>
      $(document).ready(function () {
          $('#example').DataTable();
      });
    </script> 
  </body>
</html>