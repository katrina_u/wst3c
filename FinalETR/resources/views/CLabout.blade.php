<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
  
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>

    <!-- Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500;600;700&family=Quattrocento:wght@400;700&display=swap" rel="stylesheet">

    <!-- Nav Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&family=Rubik&display=swap" rel="stylesheet">

    <title>About Us</title>

    <style>
      .bg-1 { 
        background-color: #FF9898;
      }

      @foreach ($users as $user)
      .bg-pic {
         background: radial-gradient(rgba(
              0, 0, 0, 0.90), rgba(0, 0, 0, 0.50)), url({{ url('images/'.$user->aboutus_header) }});
        background-size: cover;
        background-position: center;
        height: 73vh;
      }
      @endforeach

      body{
        background-color: #FFDCDC;
      }
      .h-cust-font {
        font-family: 'Oswald', sans-serif;
      }
      .cust-font {
        font-family: 'Quattrocento', serif;
      }
      .gen-font{
          font-family: 'Rubik', sans-serif;
      }
    </style>

   <script> 
    $(document).ready( function () {
        $('#table_id').DataTable();
    } ); 
    </script>

  </head>
  <body>
    <!-- NavBar -->
    
        <div class="container-fluid sticky-top">
          <div class="row">
            <nav class="navbar navbar-expand-sm bg-1 sticky-top gen-font p-3">
              <div class="col-sm-5">
                <div class="row">
                  <div class="col-sm-2">
                    <a class="py-5 px-2" href="Chome"><img src="images/logo_noBG.png" style="width: 70px;" alt=""></a>
                  </div>
                  <div class="border-start col-sm-3">
                     <a class="ps-2 text-decoration-none " href="Chome">
                      <h4 class="pt-1 h-cust-font text-dark">Takoyaken Takoyaki</h4>
                      </a>
                  </div>
                </div>
              </div>

              <div class="col-sm-5">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <ul class="navbar-nav justify-content-center">
                  <li class="nav-item">
                    <a class="nav-link text-dark" aria-current="page" href="Chome">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-dark" href="Cmenu">Menu</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-dark" href="Cbranches">Branches</a>
                  </li>
                  <li class="nav-item">
                  <a class="nav-link text-dark" href="Clocation">Location</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-dark" href="Ccontact">Contact Us</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link text-white" href="Cabout">About Us</a>
                  </li> 
                  </ul>
              </div>
              <div class="col-sm-1">
                <a href="shopping-cart"><i class="fas fa-shopping-cart text-dark"></i></a>
              </div>
            </nav>
          </div>
        </div>
        
        
    <!-- NavBar -->

    <!-- Body -->

    <div class="container-fluid ">
      <div class="row">
        <div class="bg-pic p-0 text-center text-white" style="padding-top: 30vh;">
            <p class="h-cust-font" style="margin-top: 20vh; font-size: 100px;">ABOUT US</p> 
            <h5 class="gen-font" style="padding-bottom: 20vh;">"Lasang Binabalikan, Takoyaking Pinipilahan"</h5>      
        </div>
      </div>

    @foreach ($users as $user)
      <div class="row p-5 justify-content-center">
        <div class="card mb-3" style="max-width: 800px;">
          <div class="row g-0">
            <div class="col-md-4 py-3">
              <img src="{{ url('images/'.$user->aboutus_photo) }}" class="img-fluid rounded-start" alt="...">
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h1 class="card-title pb-3 h-cust-font">OUR STORY</h1>
                <p class="card-text gen-font">{{ $user->aboutus_desc }}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
  @endforeach
    </div>
    <!-- Body -->

    <!-- Footer -->
    <footer class=" bg-1 text-center text-white">
      <div class="container-fluid p-4" style="background-color: rgba(0, 0, 0, 0.2);">
        <div class="row">
          <div class="col-sm-6 p-3">
            <div class="text-center pt-3">
                © 2022 Copyright:
              <a class="text-white" href="https://www.facebook.com/takoyakenurdaneta" target="_blank">Takoyaken Takoyaki</a>
              <p>Developed by Katrina G. Urbano</p>
            </div>
          </div>
          <div class="col-sm-6 p-3">
            <a class="btn btn-outline-dark btn-floating ms-5 mt-4" href="https://www.facebook.com/takoyakenurdaneta" role="button" target="_blank"><i class="fab fa-facebook-f"></i> Facebook</a>

            <a class="btn btn-outline-dark btn-floating ms-5 mt-4" href="#" role="button"><i class="fab fa-instagram"></i> Instagram</a>

            <a class="btn btn-outline-dark btn-floating ms-5 mt-4" href="#" role="button"><i class="fab fa-twitter"></i> Twitter</a>
          </div>
        </div>
      </div>     
    </footer>
    <!-- Footer -->

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>

