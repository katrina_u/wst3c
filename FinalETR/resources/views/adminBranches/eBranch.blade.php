<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <!-- Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500;600;700&family=Quattrocento:wght@400;700&display=swap" rel="stylesheet">


    <!-- Nav Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&family=Rubik&display=swap" rel="stylesheet">

    <title>Homepage</title>

    <style>
      .bg-1 { 
        background-color: #c9686a;
      }

      .f-col{
        color: #FFDCDC;
      }
      body{
        background-color: #FFDCDC;
      }

      .h-cust-font {
          font-family: 'Oswald', sans-serif;
      }

      .cust-font {
          font-family: 'Quattrocento', serif;
      }

      .gen-font{
          font-family: 'Rubik', sans-serif;
      }

      .bg-pic {
         background: radial-gradient(rgba(
              0, 0, 0, 0.25), rgba(0, 0, 0, 0.50)), url('/images/flavor_4.jpg');
        background-size: cover;
        background-position: center;
        height: 60vh;
      }
      .truncate {
        max-width:500px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    </style>

   

  </head>
  <body>
    <!-- NavBar -->

<nav class="navbar navbar-expand-sm bg-1 justify-content-center sticky-top">
    <a class="navbar-brand text-white pe-5" href="#">Hello, {{ $data->fname }}</a>
    <a class="navbar-brand text-white" href="ADMINbranches">Branches</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link text-dark" href="view-ADMINhomepage">Homepage</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINmenu">Menu</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" aria-current="page" href="view-ADMINlocation">Location</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="view-ADMINabout">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINcontact">Contact Us</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link text-dark" href="logout">Log out</a>
      </li>
    </ul>
</nav>

    <!-- NavBar -->

    <!-- Body -->
    <div class="container-fluid">
      <div class="row bg-1 mt-2" style="margin-bottom: 0px;">
        <div class="col-sm-4">
          <div class="d-grid gap-2 d-md-block mx-5 mt-3 pt-3">
            <a href="/view-ADMINbranches" class="text-white text-decoration-none">
              <button class="btn btn-dark border-cust rounded-pill" type="button"><i class="fas fa-angle-left fa-1x"></i>  Back </button>
            </a>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="row">
            <p class="gen-font pt-3 text-center" style="font-size: 5vh; font-weight: bold; margin: 0px;"> EDIT</p>
              <h6 class="gen-font pb-3 text-center f-col">Branch</h6>
          </div>
        </div>
        <div class="col-sm-4 text-end">
          <div class="d-grid gap-2 d-md-block mt-3 mx-5 pt-3">
          </div>
        </div>
      </div>
    </div>
     <div class="container-fluid px-5 pt-3">
      <div class="row px-5">
        <div class="col">
          @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible fade show border border-dark" role="alert" enctype="multipart/form-data">
              {{ Session::get('success') }}
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
          @endif

          @if(Session::has('fail'))
            <div class="alert alert-secondary alert-dismissible fade show border border-dark" role="alert">
              {{ Session::get('fail') }}
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
          @endif           
          <form action="/eBranch/<?php echo $users[0]->id; ?>" class="border border-dark p-4 rounded" method="post" enctype="multipart/form-data">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">  
            <div class="form-floating mb-3">
              <input type="file" class="form-control" id="floatingInput" placeholder="name@example.com" name="bg_photo" value="<?php echo $users[0]->bg_photo; ?>" required>
              <label for="floatingInput">Photo</label>
            </div>
            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingInput" placeholder="name@example.com" name="b_reg" value="<?php echo $users[0]->b_reg; ?> " required>
              <label for="floatingInput">Region</label>
            </div>
            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingPassword" placeholder="Password" name="b_prov" value="<?php echo $users[0]->b_prov; ?>" required>
              <label for="floatingInput">Province</label>
            </div>
            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingPassword" placeholder="Password" name="b_city" value="<?php echo $users[0]->b_city; ?>" required>
              <label for="floatingInput">City</label>
            </div>
            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingPassword" placeholder="Password" name="b_brgy" value="<?php echo $users[0]->b_brgy; ?>" required>
              <label for="floatingInput">Barangay</label>
            </div>
             <div class="form-floating mb-3">
              <input type="number" class="form-control" id="floatingPassword" placeholder="Password" name="b_postal" value="<?php echo $users[0]->b_postal; ?>" required>
              <label for="floatingInput">Postal Code</label>
            </div>
            <div class="text-center w-70">
              <button type="submit" class="btn bg-1 mt-4 px-5 py-1 text-white">SAVE</button>    
            </div>
          </form>
        </div>        
      </div>
    </div>

    <!-- Body -->

    <!-- Footer -->
    <footer class="bg-1 text-center text-white">
      <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2); margin-top: 8%;">
        © 2022 Copyright:
        <a class="text-white" href="https://mdbootstrap.com/">Katrina G. Urbano</a>
      </div>
    </footer>
    <!-- Footer -->

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>