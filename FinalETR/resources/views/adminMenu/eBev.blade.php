<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <!-- Datatable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- Bootstrap Growl -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-growl/1.0.0/jquery.bootstrap-growl.min.js" integrity="sha512-pBoUgBw+mK85IYWlMTSeBQ0Djx3u23anXFNQfBiIm2D8MbVT9lr+IxUccP8AMMQ6LCvgnlhUCK3ZCThaBCr8Ng==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <!-- Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500;600;700&family=Quattrocento:wght@400;700&display=swap" rel="stylesheet">


    <!-- Nav Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&family=Rubik&display=swap" rel="stylesheet">


    <script>
    $(document).ready(function() {
    $('#example').DataTable( {
        select: true
        } );
    } );
    </script>

    <title>Menu Beverage EF - Admin</title>

    <style>
      .bg-1 { 
        background-color: #c9686a;
      }
      .f-col{
        color: #FFDCDC;
      }

      body{
        background-color: #FFDCDC;
      }

      .h-cust-font {
          font-family: 'Oswald', sans-serif;
      }

      .cust-font {
          font-family: 'Quattrocento', serif;
      }

      .gen-font{
          font-family: 'Rubik', sans-serif;
      }

      .bg-pic {
         background: radial-gradient(rgba(
              0, 0, 0, 0.25), rgba(0, 0, 0, 0.50)), url('/images/flavor_4.jpg');
        background-size: cover;
        background-position: center;
        height: 60vh;
      }

      .border-cust{ 
        border-radius: 50px;
      }
    </style>

   

  </head>
  <body>
    <!-- NavBar -->

  <nav class="navbar navbar-expand-sm bg-1 justify-content-center">
    <a class="navbar-brand text-white pe-5" href="#">Hello, {{ $data->fname }}</a>
    <a class="navbar-brand text-white" href="ADMINmenu">Menu</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link text-dark" href="view-ADMINhomepage">Homepage</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link text-dark" aria-current="page" href="reg">Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" aria-current="page" href="view-ADMINlocation">Location</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINbranches">Branches</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="view-ADMINabout">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINcontact">Contact Us</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link text-dark" href="logout">Log out</a>
      </li>
    </ul>
</nav>

    <!-- NavBar -->

    <!-- Body -->

      <div class="container-fluid">
        <div class="row bg-1 mt-2" style="margin-bottom: 0px;">
          <div class="col-sm-4">
            <div class="d-grid gap-2 d-md-block mx-5 mt-3 pt-3">
              <a href="/view-ADMINbev_menu" class="text-white text-decoration-none">
                <button class="btn btn-dark border-cust rounded-pill" type="button"><i class="fas fa-angle-left fa-1x"></i>  Back </button>
              </a>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="row">
              <p class="gen-font pt-3 text-center" style="font-size: 5vh; font-weight: bold; margin: 0px;"> EDIT</p>
              <h6 class="gen-font pb-3 text-center f-col">Beverage Product Detail</h6>
            </div>
          </div>
        
          <div class="col-sm-4 text-end">
            <div class="d-grid gap-2 d-md-block mt-3 mx-5 pt-3">
              <a href="/view-ADMINbev_menu" class="text-white text-decoration-none">
              <button class="btn btn-dark border-cust rounded-pill" type="button"><i class="fas fa-list-alt"></i>  Records</button> 
              </a>
            </div>
          </div>
        </div>
      </div>

     <div class="container-fluid px-5 pt-3">
      <div class="row px-5">
        <div class="col">
           <!-- alert -->
          @if(Session::has('success'))
          <div class="alert alert-success alert-dismissible fade show border border-dark" role="alert">
            {{ Session::get('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
          @endif

          @if(Session::has('fail'))
          <div class="alert alert-secondary alert-dismissible fade show border border-dark" role="alert">
            {{ Session::get('fail') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
          @endif
         
         
          <form action="/eBev/<?php echo $users[0]->id; ?>" class="border border-dark p-4 rounded mb-5" method="post" enctype="multipart/form-data" style="background-color: white;">
          <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">  
            <div class="form-floating mb-3">
              <input type="file" class="form-control" id="floatingPassword" name="bev_photo" value="<?php echo $users[0]->bev_photo; ?>" required>
              <label for="floatingInput">Beverage Photo</label>
            </div>

            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingPassword" name="bev_name" value="<?php echo $users[0]->bev_name; ?>" required>
              <label for="floatingInput">Beverage Name</label>
            </div>

            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingPassword" name="bev_price" value="<?php echo $users[0]->bev_price; ?>" required>
              <label for="floatingInput">Beverage Price</label>
            </div>

            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingPassword" name="bev_desc" value="<?php echo $users[0]->bev_desc; ?>" required>
              <label for="floatingInput">Beverage Description</label>
            </div>

            <div class="text-center w-70">
              <button type="submit" class="btn bg-1 mt-4 px-5 py-1 text-white">SAVE</button>    
            </div>
          </form>
        </div>        
      </div>
    </div>
    <!-- Body -->

    <!-- Footer -->
    <footer class="bg-1 text-center text-white">
      <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2); margin-top: 4%;">
        © 2022 Copyright:
        <a class="text-white" href="https://mdbootstrap.com/">Katrina G. Urbano</a>
      </div>
    </footer>
    <!-- Footer -->

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <!-- Toastr -->  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  </body>
</html>