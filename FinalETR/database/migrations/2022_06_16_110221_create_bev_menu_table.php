<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bev_menu', function (Blueprint $table) {
            $table->id();
            $table->string('bev_photo');
            $table->string('bev_name');
            $table->string('bev_price');
            $table->string('bev_desc', 500);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bev_menu');
    }
};
