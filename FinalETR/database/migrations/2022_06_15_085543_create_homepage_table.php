<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homepage', function (Blueprint $table) {
            $table->id();
            $table->string('sBranch');
            $table->string('sPhrase');
            $table->string('sHeaderPhoto');
            $table->string('sCommit');
            $table->string('sStoryPhoto');
            $table->string('sProPhoto');
            $table->string('sProDesc');
            $table->string('sLocation');
            $table->string('sLocPhoto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homepage');
    }
};
