<?php

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Login With Google</title>
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  	<link href="css/style.css" rel="stylesheet">
</head>
<body>
	<div class="login-form">
		<form action="" method="post">	
			<div class="text-center social-btn">
				<div class="container ">
                    <div class="row">
                        <div class="col">
                            <p>Name: <strong>Katrina G. Urbano</strong></p>
                        </div>
                        <div class="col">
                            <p>Day: <strong><?php echo date("F j, Y");?></strong></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p>Year & Section: <strong>3rd Year - IT3C</strong></p>
                        </div>
                        <div class="col">
                            <p>Time: <strong><?php echo date("h:i A", strtotime("+7 hours"));?></strong></p>
                        </div>
                    </div>
                </div>
			</div>			
			
		</form>
	</div>
<div class="container" style="margin-top:30px">
	  <div class="row">
		<div class="col text-center">		  	
					<table class="table table-hover table-bordered">
						<tbody>
							<tr>
								<td class="bg-danger text-white" colspan="2">Welcome in <strong>Google+</strong>!</td>
							</tr>
							
						</tbody>
					</table>

            <div class="container">
            	<div class="row">
            		<div class="col text-center">
            			<a href="g-logout.php">
						    <button type="button" class="btn btn-info">
						    	LOGOUT
						    </button>
						</a>	
            		</div>
            		
            	</div>
            </div>

    
        </div>
    </div>
</div>
</body>
</html>