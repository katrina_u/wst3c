<?php
require_once(__DIR__.'/Facebook/autoload.php');

define('APP_ID', '491726179007135');
define('APP_SECRET', 'c4faebc93c8a4a3f0926f6b85f48123d');
define('API_VERSION', 'v2.5');
define('FB_BASE_URL', 'ENTER_URL');

define('BASE_URL', 'http://localhost/ELEC/act2/');

if(!session_id()){
    session_start();
}

$fb = new Facebook\Facebook([
 'app_id' => APP_ID,
 'app_secret' => APP_SECRET,
 'default_graph_version' => API_VERSION,
]);


$fb_helper = $fb->getRedirectLoginHelper();

try {
    if(isset($_SESSION['facebook_access_token']))
		{$accessToken = $_SESSION['facebook_access_token'];}
	else
		{$accessToken = $fb_helper->getAccessToken();}
} catch(FacebookResponseException $e) {
     echo 'Facebook API Error: ' . $e->getMessage();
      exit;
} catch(FacebookSDKException $e) {
    echo 'Facebook SDK Error: ' . $e->getMessage();
      exit;
}
?>