<?php

include 'g-config.php';
if (isset($_SESSION['access_token'])) {
	header('Location: g-index.php');
	exit();
}

$loginURL = $gClient->createAuthUrl();
require_once 'fb_config.php';


$permissions = ['email']; 

if (isset($accessToken))
{
	if (!isset($_SESSION['facebook_access_token'])) 
	{
		$_SESSION['facebook_access_token'] = (string) $accessToken;
		
		$oAuth2Client = $fb->getOAuth2Client();
		
		$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
		$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
		
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	} 
	else 
	{
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	}
	
	if (isset($_GET['code'])) 
	{
		header('Location: fb-index.php');
	}
	
	
	try {
		$fb_response = $fb->get('/me?fields=name,first_name,last_name,email');
		$fb_response_picture = $fb->get('/me/picture?redirect=false&height=200');
		
		$fb_user = $fb_response->getGraphUser();
		$picture = $fb_response_picture->getGraphUser();
		
		$_SESSION['fb_user_id'] = $fb_user->getProperty('id');
		$_SESSION['fb_user_name'] = $fb_user->getProperty('name');
		$_SESSION['fb_user_email'] = $fb_user->getProperty('email');
		$_SESSION['fb_user_pic'] = $picture['url'];
		
		
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		echo 'Facebook API Error: ' . $e->getMessage();
		session_destroy();
		// redirecting user back to app login page
		header("Location: index.php");
		exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		echo 'Facebook SDK Error: ' . $e->getMessage();
		exit;
	}
} 
else 
{	
	// replace your website URL same as added in the developers.Facebook.com/apps e.g. if you used http instead of https and you used
	$fb_login_url = $fb_helper->getLoginUrl('http://localhost/ELEC/act2/fb-index.php', $permissions);
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login with Facebook</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  
  <link href="<?php echo BASE_URL; ?>css/style.css" rel="stylesheet">
  
</head>
<body>


<?php if(isset($_SESSION['fb_user_id'])): ?>
	<div class="container" style="margin-top:30px">
	  <div class="row">
		<div class="col text-center">

		  	
					<table class="table table-hover table-bordered">
						<tbody>
							<tr>
								<td class="bg-primary text-white" colspan="2">Welcome,<strong> <?php echo $_SESSION['fb_user_name']; ?></strong>, in Facebook!</td>
							</tr>
							<tr>
								<td>Name</td>
								<td><?php echo $_SESSION['fb_user_name']; ?></td>
							</tr>
							<tr>
								<td>Email</td>
								<td><?php echo $_SESSION['fb_user_email']; ?></td>
							</tr>
						</tbody>
					</table>

			<div class="container">
            	<div class="row">
            		<div class="col text-center">
            			<a href="logout.php">
						    <button type="button" class="btn btn-info">
						    	LOGOUT
						    </button>
						</a>	
            		</div>
            		
            	</div>
            </div>		  
		</div>
	  </div>
	</div>
</body>
</html>