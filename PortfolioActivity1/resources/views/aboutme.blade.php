<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>About me & Digital Resume</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.6.0/css/all.min.css">
	<style>
		.nav-bg{
			background-color: #c5cae9;
		}	

		.opacity-aboutme1 {
			opacity: 0.5;
		}

		.img-dark{
			background-color: black;
		}
		
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light nav-bg sticky-top">
	  <div class="container-fluid">
	    <a class="nav-link active text-dark" href="/">Homepage</a>
	    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	      <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse" id="navbarSupportedContent">
	      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
	        <li class="nav-item">
	          <a class="navbar-brand" aria-current="page" href="/aboutme">About Me</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="/reg">Registration</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="/login">Login Form</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="/gallery">Gallery</a>
	        </li>
	      </ul>
	      <form class="d-flex">
	        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
	        <button class="btn btn-outline-light" type="submit">Search</button>
	      </form>
	    </div>
	  </div>
	</nav>

	<div class="container-fluid">
		<div class="row">
			<div class="img-dark d-flex flex-column">
				<img class="opacity-aboutme1" src="{{ URL('images/sunflower_bg.jpg') }}" alt="">
			</div>
		</div>
	</div>

<center><hr style="width:50%; height: 5px; color: red;"></center>
<div class="container">
	<div class="row">
		<div class="col-md-3">
		</div>
		<div class="col-md-6 text-center">
			<p class="h1">A GLIMPSE OF ME</p>
			<div class="card">
			  <div class="card-body lh-lg" style=" text-align: justify;">
			    Hi! My name is Katrina, just got 21 years old this year. 
			    I am the youngest in the family.
			    I love flowers, particularyl sunflowers! I love dogs even though I have asthma. What I love about them
			    is when I sniff their fur. Christ, that's heaven. I have aspins and lhasa poo dogs. I play online games. I love 
			    interacting with other people. But I feel uneasy when an opportunity is present. I get so overwhelm and start 
			    overthinking.

			    <br><br>
			    Btw, I sell game credits. 
			  </div>
			</div>
		</div>
		<div class="col-md-3">
		</div>
	</div>
</div>
<center><hr style="width:50%; height: 5px; color: red;"></center>

<center><hr style="width:50%; height: 5px; color: red;"></center>
<div class="container">
	<div class="row">
		<div class="col text-center">
			<p class="h1">DIGITAL RESUME</p>
		</div>
	</div>
</div>
<center><hr style="width:50%; height: 5px; color: red;"></center>

<div class="container">
	<div class="row">
		<div class="col text-center">
			<img class="w-75" src="{{ URL('images/digitalresume.png') }}" alt="">
		</div>
	</div>
</div>


<footer class="nav-bg text-center text-white mt-5">
  <!-- Grid container -->
  <div class="container p-4 pb-0">
    <!-- Section: Social media -->
    <section class="mb-4">
      <!-- Facebook -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-facebook-f"></i
      ></a>

      <!-- Twitter -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-twitter"></i
      ></a>

      <!-- Google -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-google"></i
      ></a>

      <!-- Instagram -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-instagram"></i
      ></a>

      <!-- Linkedin -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-linkedin-in"></i
      ></a>

      <!-- Github -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-github"></i
      ></a>
    </section>
    <!-- Section: Social media -->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2022 Copyright:
    <a class="text-white" href="">KATRINA</a>
  </div>
  <!-- Copyright -->
</footer>

</body>
</html>