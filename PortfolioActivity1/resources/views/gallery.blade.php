<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Gallery</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.6.0/css/all.min.css">
	<style>
		.nav-bg{
			background-color: #c5cae9;
		}	
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light nav-bg sticky-top">
	  <div class="container-fluid">
	    <a class="nav-link active text-dark" href="/">Homepage</a>
	    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	      <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse" id="navbarSupportedContent">
	      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="/aboutme">About Me</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="/reg">Registration</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="/login">Login Form</a>
	        </li>
	        <li class="nav-item">
	          <a class="navbar-brand" aria-current="page" href="/gallery">Gallery</a>
	        </li>
	      </ul>
	      <form class="d-flex">
	        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
	        <button class="btn btn-outline-light" type="submit">Search</button>
	      </form>
	    </div>
	  </div>
	</nav>

<div class="container">
	<div class="row">
		<figure class="text-center mt-5">
		  <blockquote class="blockquote">
		    <p>Home is where you are loved the most and act the worst.</p>
		  </blockquote>
		  <figcaption class="blockquote-footer">
		    Marjorie Pay Hinkley <cite title="Source Title"></cite>
		  </figcaption>
		</figure>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="col d-flex justify-content-center">
				<div class="card mt-4 mb-4" style="width: 18rem;">
				  <img class="card-img-top" src="{{ URL('images/pic4.jpg') }}" alt="Card image cap">
				  <div class="card-body">
				    <h5 class="card-title">December 24, 2021</h5>
				    <p class="card-text">63rd birthday of my mother!</p>
				  </div>
				</div>
			</div>

			<div class="col d-flex justify-content-center">
				<div class="card mt-4 mb-4" style="width: 18rem;">
				  <img class="card-img-top" src="{{ URL('images/pic10.jpg') }}" alt="Card image cap">
				  <div class="card-body">
				    <h5 class="card-title">January 1, 2022</h5>
				    <p class="card-text">We vibin'!</p>
				  </div>
				</div>
			</div>

			<div class="col d-flex justify-content-center">
				<div class="card mt-4 mb-4" style="width: 18rem;">
				  <img class="card-img-top" src="{{ URL('images/pic2.jpg') }}" alt="Card image cap">
				  <div class="card-body">
				    <h5 class="card-title">December 24, 2021</h5>
				    <p class="card-text">Happy birthday, ma!</p>
				  </div>
				</div>
			</div>


		</div>
		<div class="col-md-4">
			<div class="col d-flex justify-content-center">
				<div class="card mt-4 mb-4" style="width: 18rem;">
				  <img class="card-img-top" src="{{ URL('images/pic6.jpg') }}" alt="Card image cap">
				  <div class="card-body">
				    <h5 class="card-title">December 24, 2021</h5>
				    <p class="card-text">Almost got locked up in SM Rosales.</p>
				  </div>
				</div>
			</div>

			<div class="col d-flex justify-content-center">
				<div class="card mt-4 mb-4" style="width: 18rem;">
				  <img class="card-img-top" src="{{ URL('images/pic11.jpg') }}" alt="Card image cap">
				  <div class="card-body">
				    <h5 class="card-title">January 1, 2022</h5>
				    <p class="card-text">Show those dimples!</p>
				  </div>
				</div>
			</div>

			<div class="col d-flex justify-content-center">
				<div class="card mt-4 mb-4" style="width: 18rem;">
				  <img class="card-img-top" src="{{ URL('images/pic3.jpg') }}" alt="Card image cap">
				  <div class="card-body">
				    <h5 class="card-title">December 24, 2021</h5>
				    <p class="card-text">I'm hungry</p>
				  </div>
				</div>
			</div>

		</div>
		<div class="col-md-4">
			<div class="col d-flex justify-content-center">
				<div class="card mt-4 mb-4" style="width: 18rem;">
				  <img class="card-img-top" src="{{ URL('images/pic5.jpg') }}" alt="Card image cap">
				  <div class="card-body">
				    <h5 class="card-title">January 1, 2020</h5>
				    <p class="card-text">Welcome 2022!</p>
				  </div>
				</div>
			</div>

			<div class="col d-flex justify-content-center">
				<div class="card mt-4 mb-4" style="width: 18rem;">
				  <img class="card-img-top" src="{{ URL('images/pic12.jpg') }}" alt="Card image cap">
				  <div class="card-body">
				    <h5 class="card-title">January 1, 2022</h5>
				    <p class="card-text">63rd birthday of my mother!</p>
				  </div>
				</div>
			</div>

			<div class="col d-flex justify-content-center">
				<div class="card mt-4 mb-4" style="width: 18rem;">
				  <img class="card-img-top" src="{{ URL('images/pic7.jpg') }}" alt="Card image cap">
				  <div class="card-body">
				    <h5 class="card-title">January 1, 2022</h5>
				    <p class="card-text">Halu kuya!</p>
				  </div>
				</div>
			</div>

		</div>
	</div>
</div>

<footer class="nav-bg text-center text-white mt-5">
  <!-- Grid container -->
  <div class="container p-4 pb-0">
    <!-- Section: Social media -->
    <section class="mb-4">
      <!-- Facebook -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-facebook-f"></i
      ></a>

      <!-- Twitter -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-twitter"></i
      ></a>

      <!-- Google -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-google"></i
      ></a>

      <!-- Instagram -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-instagram"></i
      ></a>

      <!-- Linkedin -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-linkedin-in"></i
      ></a>

      <!-- Github -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-github"></i
      ></a>
    </section>
    <!-- Section: Social media -->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
 <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2022 Copyright:
    <a class="text-white" href="">KATRINA</a>
  </div>
  <!-- Copyright -->
</footer>

</body>
</html>