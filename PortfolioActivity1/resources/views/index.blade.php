<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Homepage</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.6.0/css/all.min.css">
	<style>
		.nav-bg{
			background-color: #c5cae9;
		}	

		.img-size {
			width: 350px;

		}

		.bg-custom {
			background-color:  black;'
			padding: 50px;
		}

		.car-size{
			width: 300px;
		}


		.carousel .item {
		  height: 300px;
		}

	</style>
</script>  
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light nav-bg sticky-top">
	  <div class="container-fluid">
	    <a class="navbar-brand" href="/">Homepage</a>
	    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	      <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse" id="navbarSupportedContent">
	      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="/aboutme">About Me</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="/reg">Registration</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="/login">Login Form</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="/gallery">Gallery</a>
	        </li>
	      </ul>
	      <form class="d-flex">
	        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
	        <button class="btn btn-outline-light" type="submit">Search</button>
	      </form>
	    </div>
	  </div>
	</nav>

	<div class="container">
		<div class="row">
			<div class="col-md-6 mt-3 mb-5">
			<!-- Carousel -->
			<div id="demo" class="carousel slide" data-bs-ride="carousel">

			  <!-- Indicators/dots -->
			  <div class="carousel-indicators">
			    <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
			    <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
			    <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
			  </div>
			  
			  <!-- The slideshow/carousel -->
			  <div class="carousel-inner">
			    <div class="carousel-item active">
			      <img src="{{ URL('images/pic4.jpg') }}" alt="Los Angeles" class="d-block w-100">
			    </div>
			    <div class="carousel-item">
			      <img src="{{ URL('images/pic1.jpg') }}" alt="Chicago" class="d-block w-100">
			    </div>
			    <div class="carousel-item">
			      <img src="{{ URL('images/pic3.jpg') }}" alt="New York" class="d-block w-100 mt-5 mb-5">
			    </div>
			  </div>
			  
			  <!-- Left and right controls/icons -->
			  <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
			    <span class="carousel-control-prev-icon"></span>
			  </button>
			  <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
			    <span class="carousel-control-next-icon"></span>
			  </button>
			</div>
			<!-- <div class="col d-flex justify-content-center">
				<div class="card mt-4 mb-4" style="width: 18rem;">
				  <img class="card-img-top" src="{{ URL('images/pic4.jpg') }}" alt="Card image cap">
				  <div class="card-body">
				    <h5 class="card-title">December 24, 2021</h5>
				    <p class="card-text">63rd birthday of my mother!</p>
				  </div>
				</div>
			</div> -->
			</div>

			<div class="col-md-6">
				<div class="container">
					<div class="row">
						<div class="col pt-5">
							<div class="card mb-5 mt-5">
							  <div class="card-body lh-lg" style=" text-align: justify;">
							    <p class="h1">Welcome!</p>				
							<p class="h6 lh-lg">I am Katrina, I have nothing to say. But you are welcome in this website, so hello there! Have a good day!</p>
							<br>
							<p class="h6 lh-lg">The pictures in your right hand sided are me and my family. </p>
							  </div>
							</div>							
						</div>						
					</div>
				</div>				
			</div>
		</div>
	</div>


<footer class="nav-bg text-center text-white mt-5">
  <!-- Grid container -->
  <div class="container p-4 pb-0">
    <!-- Section: Social media -->
    <section class="mb-4">
      <!-- Facebook -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-facebook-f"></i
      ></a>

      <!-- Twitter -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-twitter"></i
      ></a>

      <!-- Google -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-google"></i
      ></a>

      <!-- Instagram -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-instagram"></i
      ></a>

      <!-- Linkedin -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-linkedin-in"></i
      ></a>

      <!-- Github -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-github"></i
      ></a>
    </section>
    <!-- Section: Social media -->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2022 Copyright:
    <a class="text-white" href="">KATRINA</a>
  </div>
  <!-- Copyright -->
</footer>

</body>
</html>