<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.6.0/css/all.min.css">
	<style>
		.nav-bg{
			background-color: #c5cae9;
		}	

		.mar { 
			margin-bottom: 180px;
		}

	</style>
	<script>
		var myModal = document.getElementById('myModal')
		var myInput = document.getElementById('myInput')

		myModal.addEventListener('shown.bs.modal', function () {
		  myInput.focus()
		})		
	</script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light nav-bg sticky-top">
	  <div class="container-fluid">
	    <a class="nav-link active text-dark" href="/">Homepage</a>
	    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	      <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse" id="navbarSupportedContent">
	      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="/aboutme">About Me</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="/reg">Registration</a>
	        </li>
	        <li class="nav-item">
	          <a class="navbar-brand" aria-current="page" href="/login">Login Form</a>
	        </li>
	        <li class="nav-item">
	          <a class="nav-link active" aria-current="page" href="/gallery">Gallery</a>
	        </li>
	      </ul>
	      <form class="d-flex">
	        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
	        <button class="btn btn-outline-light" type="submit">Search</button>
	      </form>
	    </div>
	  </div>
	</nav>

<div class="container mar">
	<div class="row">
		<center><hr class="mt-5" style="width:50%; height: 5px; color: red;"></center>
						<div class="container">
							<div class="row">
								<div class="col-md-3">
									
								</div>
								<div class="col-md-6 text-center">
									<p class="h1">Login Form</p>
								</div>
								<div class="col-md-3">
									
								</div>
							</div>
						</div>
					<center><hr style="width:50%; height: 5px; color: red;"></center>
		<div class="col-md-3">
			
			</div>
				<div class="col-md-6">
					
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						
					</div>
					<div class="col-md-6 mt-5 text-center">
						<p class="h6 mb-3">Have an existing account?</p>
						<!-- Button trigger modal -->
						<button type="button" class="btn btn-primary " data-bs-toggle="modal" data-bs-target="#exampleModal">
						  Click me
						</button>
					</div>
					<div class="col-md-3">
						
					</div>
				</div>
			</div>
			
		</div>
		<div class="col-md-3">
			
		</div>
	</div>
</div>



<form action="">
		<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Log in</h5>
		        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		      </div>
		      <div class="modal-body">
		      	<div class="input-group mb-3">
				  <span class="input-group-text" id="basic-addon1">@</span>
				  <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
				</div>
				<div class="form-floating mb-3">
				  <input type="password" class="form-control" id="floatingPassword" placeholder="Password">
				  <label for="floatingPassword">Password</label>
				</div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary">Submit</button>
		      </div>
		    </div>
		  </div>
		</div>
	</form>


<footer class="nav-bg text-center text-white mt-5">
  <!-- Grid container -->
  <div class="container p-4 pb-0">
    <!-- Section: Social media -->
    <section class="mb-4">
      <!-- Facebook -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-facebook-f"></i
      ></a>

      <!-- Twitter -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-twitter"></i
      ></a>

      <!-- Google -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-google"></i
      ></a>

      <!-- Instagram -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-instagram"></i
      ></a>

      <!-- Linkedin -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-linkedin-in"></i
      ></a>

      <!-- Github -->
      <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
        ><i class="fab fa-github"></i
      ></a>
    </section>
    <!-- Section: Social media -->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2022 Copyright:
    <a class="text-white" href="">KATRINA</a>
  </div>
  <!-- Copyright -->
</footer>

</body>
</html>