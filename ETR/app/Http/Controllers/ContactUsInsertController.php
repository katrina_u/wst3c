<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Session;
use DB;


class ContactUsInsertController extends Controller
{
    public function insertform_contact(){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }
        return view('adminContactUs.ADcontact', compact('data'));
    }

    public function insert_contact(Request $request) 
    {
        $contype = $request->input('contype');
        $condel = $request->input('condel');
        $res = DB::insert('insert into contactus (contype, condel) values(?,?)',[$contype, $condel]);
        if($res){
            return back()->with('success', 'Contact details was successfully added!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
    }

}
