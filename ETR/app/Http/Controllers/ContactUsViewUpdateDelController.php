<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Models\User;

class ContactUsViewUpdateDelController extends Controller
{
    public function index_AdminEditContact()
    {
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }        
        $users = DB::select('select * from contactus');
        return view('adminContactUs.EditContact',['users'=>$users], compact('data'));
    }

    public function index_ClientContact(){
        $users = DB::select('select * from contactus');
        return view('CLcontact',['users'=>$users]);
    }

    public function showEditContact($id){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }    
        $users = DB::select('select * from contactus where id = ?',[$id]);
        return view('adminContactUs.eContact',['users'=>$users], compact('data'));
    }

    public function editContact(Request $request,$id){

        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }    

        $contype = $request->input('contype');
        $condel = $request->input('condel');

        $res = DB::update('update contactus set contype = ?, condel = ? where id = ?',[$contype, $condel, $id]);

        if($res){
            return back()->with('success', 'Contact details was succesfully updated!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }

        $users = DB::select('select * from contactus where id = ?',[$id]);
        return view('adminContactUs.eContact',['users'=>$users], compact('data'));
    }

    public function deleteContact($id){
        $res = DB::delete('delete from contactus where id = ?',[$id]);
        if($res){
            return back()->with('success', 'Contact details was succesfully deleted!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
    }
}
