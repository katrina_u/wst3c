<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Session;
use DB;

class HomepageInsertController extends Controller
{
    public function insertform_homepage(){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }
        return view('adminHomepage.ADhomepage', compact('data'));
    }

    public function insert_homepage(Request $request) 
    {
        $sBranch = $request->input('sBranch');
        $sPhrase = $request->input('sPhrase');
        $sHeaderPhoto = $request->input('sHeaderPhoto');
        $sCommit = $request->input('sCommit');
        $sStoryPhoto = $request->input('sStoryPhoto');
        $sProPhoto = $request->input('sProPhoto');
        $sProDesc = $request->input('sProDesc');
        $sLocation = $request->input('sLocation');
        $sLocPhoto = $request->input('sLocPhoto');
        
        $res = DB::insert('insert into homepage (sBranch, sPhrase, sHeaderPhoto, sCommit, sStoryPhoto, sProPhoto, sProDesc, sLocation, sLocPhoto) values(?,?,?,?,?,?,?,?,?)',[$sBranch, $sPhrase, $sHeaderPhoto, $sCommit, $sStoryPhoto, $sProPhoto, $sProDesc, $sLocation, $sLocPhoto]);
         if($res){
            return back()->with('success', 'Details was succesfully added!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
        
    }
}
