<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Models\User;
class BranchesViewUpdateDelController extends Controller
{
     public function index_AdminEditBranches(){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        } 
        $users = DB::select('select * from branches');
        return view('adminBranches.EditBranches',['users'=>$users], compact('data'));
    }

    public function index_ClientBranches(){
        $users = DB::select('select * from branches');
        return view('CLbranches',['users'=>$users]);
    }

    public function showEditBranch($id){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        } 
        $users = DB::select('select * from branches where id = ?',[$id]);
        return view('adminBranches.eBranch',['users'=>$users], compact('data'));
    }

    public function editBranch(Request $request,$id){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }    

        $b_reg = $request->input('b_reg');
        $b_prov = $request->input('b_prov');
        $b_city = $request->input('b_city');
        $b_brgy = $request->input('b_brgy');
        $b_postal = $request->input('b_postal');
        if($request->hasfile('bg_photo')){
            $file = $request->file('bg_photo');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images', $filename);

            $res = DB::update('update branches set bg_photo = ?, b_reg = ?, b_prov = ?, b_city = ?, b_brgy = ?, b_postal = ? where id = ?',[$filename, $b_reg, $b_prov, $b_city, $b_brgy, $b_postal, $id]);
            if($res){
                return back()->with('success', 'Branch details was succesfully updated!');
            } else{
                return back()->with('fail', 'Something went wrong');
            }
        }
        
        $users = DB::select('select * from branches where id = ?',[$id]);
        return view('adminBranches.eBranch',['users'=>$users], compact('data'));
    }

    public function deleteBranch($id){
        $res = DB::delete('delete from branches where id = ?',[$id]);
        if($res){
            return back()->with('success', 'Branch details was succesfully deleted!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
    }
}
