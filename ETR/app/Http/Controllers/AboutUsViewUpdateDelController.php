<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Models\User;

class AboutUsViewUpdateDelController extends Controller
{
    public function index_AdminEditAbout()
    {
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }        
        $users = DB::select('select * from aboutus');
        return view('adminAboutUs.EditAboutUs',['users'=>$users], compact('data'));
    }

    public function index_ClientAbout(){
        $users = DB::select('select * from aboutus');
        return view('CLabout',['users'=>$users]);
    }

    public function showEditAbout($id){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }   
        $users = DB::select('select * from aboutus where id = ?', [$id]);
        return view('adminAboutUs.eAbout',['users'=>$users], compact('data'));
    }
    
    public function editAbout(Request $request, $id){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        } 

        $aboutus_header = $request->input('aboutus_header');
        $aboutus_desc = $request->input('aboutus_desc');
        $aboutus_photo = $request->input('aboutus_photo');
        
        $res = DB::update('update aboutus set aboutus_header = ?, aboutus_photo = ?, aboutus_desc = ? where id = ?', [$aboutus_header, $aboutus_photo, $aboutus_desc, $id]);
        if($res){
            return back()->with('success', 'Details was succesfully updated!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
        $users = DB::select('select * from aboutus where id = ?', [$id]);
        return view('adminAboutUs.eAbout',['users'=>$users], compact('data'));

        
    }
    
}

