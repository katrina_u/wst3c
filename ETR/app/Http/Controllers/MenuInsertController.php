<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Session;
use DB;

class MenuInsertController extends Controller
{
     public function insertform_menuTakoyaki(){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }
        return view('adminMenu.ADmenu', compact('data'));
    }

    public function insert_menuTakoyaki(Request $request) 
    {
        if($request->hasfile('tak_photo')){
            $file = $request->file('tak_photo');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('images', $filename);
        }

        $tak_name = $request->input('tak_name');
        $tak_price = $request->input('tak_price');
        $tak_desc = $request->input('tak_desc');
        
       $res = DB::insert('insert into takoyaki_menu (tak_photo, tak_name, tak_price, tak_desc) values(?,?,?,?)',[$filename, $tak_name, $tak_price, $tak_desc]);

       if($res){
            return back()->with('success', 'Product details was successfully added!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
    }

     public function insertform_menuBev(){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }
        return view('adminMenu.ADmenubev', compact('data'));
    }

    public function insert_menuBev(Request $request) 
    {
        if($request->hasfile('bev_photo')){
            $file = $request->file('bev_photo');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('images', $filename);
        }

        $bev_name = $request->input('bev_name');
        $bev_price = $request->input('bev_price');
        $bev_desc = $request->input('bev_desc');
        
        $res = DB::insert('insert into bev_menu (bev_photo, bev_name, bev_price, bev_desc) values(?,?,?,?)',[$filename, $bev_name, $bev_price, $bev_desc]);
        if($res){
            return back()->with('success', 'Product details was successfully added!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
    }
}
