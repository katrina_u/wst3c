<?php

namespace App\Http\Controllers;
use App\Models\User;
use Session;
use DB;

use Illuminate\Http\Request;

class OrderInsertContoller extends Controller
{
   public function insert_order(Request $request) 
    {
        $user_order = $request->input('user_order');
        $user_quantity = $request->input('user_quantity');
        
        $res = DB::insert('insert into user_orders (user_order,user_quantity) values(?,?)', [$user_order, $user_quantity]);
        if($res){
            return back()->with('success', 'Order placed! Thank you for purchasing!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
    }
}
