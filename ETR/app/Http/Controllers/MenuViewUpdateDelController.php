<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Models\User;

class MenuViewUpdateDelController extends Controller
{
    public function index_ClientMenu(){
        $users = DB::select('select * from takoyaki_menu');
        return view('Cmenu', ['users'=>$users]);
    }

    public function index_ClientMenuBev(){
        $users = DB::select('select * from bev_menu');
        return view('Cmenubev', ['users'=>$users]);
    }


    public function index_AdminEditTakoyaki(){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        } 
        $users = DB::select('select * from takoyaki_menu');
        return view('adminMenu.EditTakoyaki',['users'=>$users], compact('data'));
    }

    public function index_AdminEditBev(){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        } 
        $users = DB::select('select * from bev_menu');
        return view('adminMenu.EditBev',['users'=>$users], compact('data'));
    } 


    // UPDATE DELETE tako

    public function showEditTakoyaki($id){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }    
        $users = DB::select('select * from takoyaki_menu where id = ?',[$id]);
        return view('adminMenu.eMenu',['users'=>$users], compact('data'));
    }

    public function editTakoyaki(Request $request,$id){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }    

        $tak_name = $request->input('tak_name');
        $tak_desc = $request->input('tak_desc');
        $tak_price = $request->input('tak_price');
        if($request->hasfile('tak_photo')){
            $file = $request->file('tak_photo');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('images', $filename);

            $res = DB::update('update takoyaki_menu set tak_photo = ?, tak_name = ?, tak_price = ?, tak_desc = ? where id = ?',[$filename, $tak_name, $tak_price, $tak_desc, $id]);

            if($res){
                return back()->with('success', 'Products details was succesfully updated!');
            } else{
                return back()->with('fail', 'Something went wrong');
            }
        }
        
        $users = DB::select('select * from takoyaki_menu where id = ?',[$id]);
        return view('adminMenu.eMenu',['users'=>$users], compact('data'));
    }

    public function deleteTakoyaki($id){
        $res = DB::delete('delete from takoyaki_menu where id = ?',[$id]);
        if($res){
            return back()->with('success', 'Products details was succesfully deleted!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
    }

    // UPDATE DELETE beverages

    public function showEditBev($id){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }    
        $users = DB::select('select * from bev_menu where id = ?',[$id]);
        return view('adminMenu.eBev',['users'=>$users], compact('data'));
    }

    public function editBev(Request $request,$id){
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }    

        $bev_name = $request->input('bev_name');
        $bev_desc = $request->input('bev_desc');
        $bev_price = $request->input('bev_price');
        if($request->hasfile('bev_photo')){
            $file = $request->file('bev_photo');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('images', $filename);

            $res = DB::update('update bev_menu set bev_photo = ?, bev_name = ?, bev_price = ?, bev_desc = ? where id = ?', [$filename, $bev_name, $bev_price, $bev_desc, $id]);
            if($res){
                return back()->with('success', 'Products details was succesfully updated!');
            } else{
                return back()->with('fail', 'Something went wrong');
            }
        }
        
        $users = DB::select('select * from bev_menu where id = ?',[$id]);
        return view('adminMenu.eBev',['users'=>$users], compact('data'));
    }

    public function deleteBev($id){
        $res = DB::delete('delete from bev_menu where id = ?',[$id]);
        if($res){
            return back()->with('success', 'Products details was succesfully deleted!');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
    }
}
