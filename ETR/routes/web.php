<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\ContactUsInsertController;
use App\Http\Controllers\AboutUsInsertController;
use App\Http\Controllers\LocationInsertController;
use App\Http\Controllers\BranchesInsertController;
use App\Http\Controllers\LocationViewUpdateDelController;
use App\Http\Controllers\BranchesViewUpdateDelController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\AboutUsViewUpdateDelController;
use App\Http\Controllers\ContactUsViewUpdateDelController;
use App\Http\Controllers\MenuInsertController;
use App\Http\Controllers\MenuViewUpdateDelController;
use App\Http\Controllers\HomepageInsertController;
use App\Http\Controllers\HomepageViewUpdateDelController;
use App\Http\Controllers\OrderInsertContoller;
use App\Http\Controllers\BevInsertController;
use App\Http\Controllers\CartController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CustomAuthController::class, 'login']);
Route::get('/reg', [CustomAuthController::class, 'registration']);
Route::post('/register-user', [CustomAuthController::class, 'registerUser'])->name('register-user');
Route::post('/login-user', [CustomAuthController::class, 'loginUser'])->name('login-user');
//Admin Dashboard
Route::get('/logout', [CustomAuthController::class, 'logout']);


//ORDER
Route::post('makeOrder', [OrderInsertContoller::class, 'insert_order']); //insert post
Route::post('makeOrderBev', [BevInsertController::class, 'insert_orderbev']); //insert post
Route::get('shopping-cart', [CartController::class, 'index_cart']); //insert post
Route::get('admin-shop-cart', [CartController::class, 'index_admincart']); //insert post


//CRUD ADMIN & CLIENT
//HOMEPAGE
Route::get('view-ADMINhomepage', [HomepageViewUpdateDelController::class, 'index_AdminEditHomepage']);
Route::get('eHomepage/{id}', [HomepageViewUpdateDelController::class, 'showEditHomepage']);
Route::post('eHomepage/{id}', [HomepageViewUpdateDelController::class, 'editHomepage']);

Route::get('Chome', [HomepageViewUpdateDelController::class, 'index_ClientHomepage']); //view get 

//CONTACT US
Route::get('/ADMINcontact', [ContactUsInsertController::class, 'insertform_contact']); //insert get
Route::post('/create_contact', [ContactUsInsertController::class, 'insert_contact']); //insert post
Route::get('/view-ADMINcontact', [ContactUsViewUpdateDelController::class, 'index_AdminEditContact']); //view get 
Route::get('eContact/{id}', [ContactUsViewUpdateDelController::class, 'showEditContact']);
Route::post('eContact/{id}', [ContactUsViewUpdateDelController::class, 'editContact']);
Route::get('dContact/{id}', [ContactUsViewUpdateDelController::class, 'deleteContact']);

Route::get('Ccontact', [ContactUsViewUpdateDelController::class, 'index_ClientContact']); //view get 


//ABOUT US
Route::get('/view-ADMINabout', [AboutUsViewUpdateDelController::class, 'index_AdminEditAbout']); //view get 
Route::get('eAbout/{id}', [AboutUsViewUpdateDelController::class, 'showEditAbout']); //edit get 
Route::post('eAbout/{id}', [AboutUsViewUpdateDelController::class, 'editAbout']); //edit post


Route::get('Cabout', [AboutUsViewUpdateDelController::class, 'index_ClientAbout']); //view get 



//LOCATION
Route::get('/ADMINloc', [LocationInsertController::class, 'insertform_loc']); //insert get
Route::post('/create_loc', [LocationInsertController::class, 'insert_loc']); //insert post
Route::get('view-ADMINlocation', [LocationViewUpdateDelController::class, 'index_AdminEditLocation']); //view get 
Route::get('eLocation/{id}', [LocationViewUpdateDelController::class, 'showEditLocation']);
Route::post('eLocation/{id}', [LocationViewUpdateDelController::class, 'editLocation']);

Route::get('Clocation', [LocationViewUpdateDelController::class, 'index_ClientLocation']); //view get 

//BRANCHES
Route::get('/ADMINbranches', [BranchesInsertController::class, 'insertform_branches']); //insert get
Route::post('/create_branches', [BranchesInsertController::class, 'insert_branches']); //insert post 
Route::get('view-ADMINbranches', [BranchesViewUpdateDelController::class, 'index_AdminEditBranches']); //view get 
Route::get('eBranch/{id}', [BranchesViewUpdateDelController::class, 'showEditBranch']);
Route::post('eBranch/{id}', [BranchesViewUpdateDelController::class, 'editBranch']);
Route::get('dBranch/{id}', [BranchesViewUpdateDelController::class, 'deleteBranch']);

Route::get('Cbranches', [BranchesViewUpdateDelController::class, 'index_ClientBranches']); //view get 

//MENU

	//TAKOYAKI
Route::get('/ADMINmenu', [MenuInsertController::class, 'insertform_menuTakoyaki']); //insert get
Route::post('/create_takoyaki', [MenuInsertController::class, 'insert_menuTakoyaki']); //insert post 
Route::get('view-ADMINtakoyaki_menu', [MenuViewUpdateDelController::class, 'index_AdminEditTakoyaki']); //view get 
Route::get('eMenu/{id}', [MenuViewUpdateDelController::class, 'showEditTakoyaki']);
Route::post('eMenu/{id}', [MenuViewUpdateDelController::class, 'editTakoyaki']);
Route::get('dMenu/{id}', [MenuViewUpdateDelController::class, 'deleteTakoyaki']);


	//BEV
Route::get('/ADMINbev', [MenuInsertController::class, 'insertform_menuBev']); //view get 
Route::post('/create_bev', [MenuInsertController::class, 'insert_menuBev']); //insert post 
Route::get('view-ADMINbev_menu', [MenuViewUpdateDelController::class, 'index_AdminEditBev']); //view get 
Route::get('eBev/{id}', [MenuViewUpdateDelController::class, 'showEditBev']);
Route::post('eBev/{id}', [MenuViewUpdateDelController::class, 'editBev']);
Route::get('dBev/{id}', [MenuViewUpdateDelController::class, 'deleteBev']);



Route::get('Cmenu', [MenuViewUpdateDelController::class, 'index_ClientMenu']); //view get 
Route::get('Cmenubev', [MenuViewUpdateDelController::class, 'index_ClientMenuBev']); //view get 
