<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('takoyaki_menu', function (Blueprint $table) {
            $table->id();
            $table->string('tak_photo');
            $table->string('tak_name');
            $table->string('tak_price');
            $table->string('tak_desc', 500);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('takoyaki_menu');
    }
};
