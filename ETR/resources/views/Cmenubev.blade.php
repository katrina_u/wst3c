<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <!-- Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@500;600;700&family=Quattrocento:wght@400;700&display=swap" rel="stylesheet">

    <!-- Nav Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&family=Rubik&display=swap" rel="stylesheet">
    <title>Menu - Beverages</title>

    <style>
      .bg-1 { 
        background-color: #FF9898;
      }

      .bg-2 {
        background-color: #c9686a;
      }

      .bg-pic {
         background: radial-gradient(rgba(
              0, 0, 0, 0.90), rgba(0, 0, 0, 0.50)), url('images/bev.jpg');
        background-size: cover;
        background-position: center;
        height: 73vh;  
      }

      .h-cust-font {
        font-family: 'Oswald', sans-serif;
      }

      .cust-font {
        font-family: 'Quattrocento', serif;
      }

      .gen-font{
      font-family: 'Rubik', sans-serif;
      }

      body{
        background-color: #FFDCDC;
      }

      .header-img {
         object-fit: cover;
        opacity: 0.35;
      }

      .header-bg{
        background: black;
      }

      .bg-white{
        background-color: white;
      }
    </style>
  </head>
  <body>
    <!-- NavBar -->
  <div class="container-fluid sticky-top">
          <div class="row">
            <nav class="navbar navbar-expand-sm bg-1 sticky-top gen-font p-3">
              <div class="col-sm-5">
                <div class="row">
                  <div class="col-sm-2">
                    <a class="py-5 px-2" href="Chome"><img src="images/logo_noBG.png" style="width: 70px;" alt=""></a>
                  </div>
                  <div class="border-start col-sm-3">
                     <a class="ps-2 text-decoration-none " href="Chome">
                      <h4 class="pt-1 h-cust-font text-dark">Takoyaken Takoyaki</h4>
                      </a>
                  </div>
                </div>
              </div>

              <div class="col-sm-5">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <ul class="navbar-nav justify-content-center">
                  <li class="nav-item">
                    <a class="nav-link text-dark" aria-current="page" href="Chome">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="Cmenu">Menu</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-dark" href="Cbranches">Branches</a>
                  </li>
                  <li class="nav-item">
                  <a class="nav-link text-dark" href="Clocation">Location</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-dark" href="Ccontact">Contact Us</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link text-dark" href="Cabout">About Us</a>
                  </li> 
                  </ul>
              </div>
              <div class="col-sm-1">
                <a href="shopping-cart"><i class="fas fa-shopping-cart text-dark"></i></a>
              </div>
            </nav>
          </div>

          <div class="row">
            <nav class="navbar navbar-expand-sm bg-2 sticky-top gen-font p-1">
              <div class="col-sm-5">
              </div>

              <div class="col-sm-5">
                 <nav class="gen-font" >
                    <ul class="nav justify-content-center bg-2">
                      <li class="nav-item">
                        <a class="nav-link active text-dark" aria-current="page" href="Cmenu">Takoyaki</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link text-white" href="Cmenubev">Beverages</a>
                      </li>
                    </ul>
                  </nav>
              </div>
              <div class="col-sm-1">
              </div>
            </nav>
          </div>

          <div class="row">
            <nav class="navbar navbar-expand-sm bg-2 sticky-top gen-font p-0">
              <div class="col-sm-1">
              </div>

              <div class="col-sm-10">
                 <nav class="gen-font">
                    <!-- alert -->
                        @if(Session::has('success'))
                        <div class="mt-3 alert alert-success alert-dismissible fade show border border-dark" role="alert">
                          {{ Session::get('success') }}
                          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif

                        @if(Session::has('fail'))
                        <div class="alert alert-secondary alert-dismissible fade show border border-dark" role="alert">
                          {{ Session::get('fail') }}
                          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
              </div>

              <div class="col-sm-1">
              </div>
            </nav>
          </div>
        </div>


    <!-- NavBar -->

    <!-- Body -->
    <div class="container-fluid">
      <div class="row">
        <div class="bg-pic pt-0 text-center text-white" style="padding-top: 30vh;">
            <p class="h-cust-font" style="margin-top: 16vh; font-size: 90px;">MENU</p> 
            <h6 class="gen-font px-5 mx-5">We offer, Takoyaki foods that are guaranteed made real japan ingredients,  where this popular food it all. It can serve with delicious real Octopus that can  source out here in the Philippines. Aside from delicious Octo Baby and Original  Octo Bits, Tako Tako Queen also serves other delicious variant such as Crab and  Corn, Bacon and Cheese. We also serve drinks like Red Iced Tea. And now, we  have newly added menu, All Meat – Scallop</h6> 
            <div class="mb-3 text-center mt-3 d-grid gap-2 col-2 mx-auto w-70">
              <button type="submit" class="fw-bold btn btn-light rounded-pill bg-1 text-center"  data-bs-toggle="modal" data-bs-target="#exampleModal">ORDER HERE</button>
            </div>
        </div>
      </div>
      <div class="row text-start">
        <div class="col-sm pt-5 pb-5 gen-font text-center">
          <h3 class="px-5">Summer is brighter with treats!</h3>
        </div>
      </div>

      <div class="row bg-1">
        <p class="gen-font pt-4 pb-2 text-center" style="font-size: 10vh; font-weight: bold;">BEVERAGE</p>
      </div>
      <div class="row p-5">
        @foreach ($users as $user)
        <div class="col-sm-6">
          <div class="card mt-3 mb-3 border border-dark">
            <div class="row g-0">
              <div class="col-md-4">
                <img src="{{ url('images/'.$user->bev_photo) }}" class="img-thumbnail" alt="...">
              </div>
              <div class="col-md-6">
                <div class="card-body">
                  <h5 class="card-title gen-font">{{ $user->bev_name }}</h5>
                  <p class="card-text gen-font">{{ $user->bev_desc }}</p>
                  <p class="card-text"><small class="text-muted">₱ {{ $user->bev_price }}.00</small></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>       
    </div>
      </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Make an Order</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form action="makeOrder" method="post" enctype="multipart/form-data" style="background-color: white;">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

                <div class="form-group mb-3">
                  <label for="exampleFormControlSelect1" class="form-label">Beverage</label>
                  <select select class="form-select" aria-label="Default select example" id="exampleFormControlSelect1" name="user_order_bev"> 
                    @foreach($users as $user)
                      <option value="{{ $user->bev_name}}">{{ $user->bev_name }}</option>
                    @endforeach
                  </select>
                </div>

                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">Quantity</label>
                  <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type="number" class="form-control" pattern="[0-5]" id="timer02_min2"  name="user_quantity_bev" min="1" max="9" maxlength="1" >

                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  <button type="submit" class="btn bg-1 text-white">Save changes</button>
                </div>          
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Body -->

    <!-- Footer -->
    <footer class=" bg-1 text-center text-white">
      <div class="container-fluid p-4" style="background-color: rgba(0, 0, 0, 0.2);">
        <div class="row">
          <div class="col-sm-6 p-3">
            <div class="text-center pt-3">
                © 2022 Copyright:
              <a class="text-white" href="https://www.facebook.com/takoyakenurdaneta">Takoyaken Takoyaki</a>
              <p>Developed by Katrina G. Urbano</p>
            </div>
          </div>
          <div class="col-sm-6 p-3">
            <a class="btn btn-outline-dark btn-floating ms-5 mt-4" href="https://www.facebook.com/takoyakenurdaneta" role="button"><i class="fab fa-facebook-f"></i> Facebook</a>

            <a class="btn btn-outline-dark btn-floating ms-5 mt-4" href="#" role="button"><i class="fab fa-instagram"></i> Instagram</a>

            <a class="btn btn-outline-dark btn-floating ms-5 mt-4" href="#" role="button"><i class="fab fa-twitter"></i> Twitter</a>
          </div>
        </div>
      </div>     
    </footer>
    <!-- Footer -->

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>