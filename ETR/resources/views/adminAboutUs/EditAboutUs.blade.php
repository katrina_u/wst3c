<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <!-- Datatable -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    
    <!-- Datatable Responsive -->
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.dataTables.min.css">

    <title>About Us DT - Admin</title>

    <style>
      .bg-1 { 
        background-color: #c9686a;
      }

      
      .f-col{
        color: #FFDCDC;
      }

      body{
        background-color: #FFDCDC;
      }

      .h-cust-font {
          font-family: 'Oswald', sans-serif;
      }

      .cust-font {
          font-family: 'Quattrocento', serif;
      }

      .gen-font{
          font-family: 'Rubik', sans-serif;
      }

      .bg-pic {
         background: radial-gradient(rgba(
              0, 0, 0, 0.25), rgba(0, 0, 0, 0.50)), url('/images/flavor_4.jpg');
        background-size: cover;
        background-position: center;
        height: 60vh;
      }
      .truncate {
        max-width:500px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    </style>

   

  </head>
  <body>
    <!-- NavBar -->

<nav class="navbar navbar-expand-sm bg-1 justify-content-center">
    <a class="navbar-brand text-white" href="#">Hello, {{ $data->fname }}</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link text-dark" href="view-ADMINhomepage">Homepage</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINmenu">Menu</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="admin-shop-cart">Shopping Cart</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINbranches">Branches</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" aria-current="page" href="view-ADMINlocation">Location</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="view-ADMINabout">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-dark" href="ADMINcontact">Contact Us</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link text-dark" href="logout">Log out</a>
      </li>
    </ul>
</nav>

    <!-- NavBar -->
 
    <!-- Body -->

      <div class="d-grid gap-2 d-md-block m-5 ">
        <a href="Cabout" class="text-white text-decoration-none" target="_blank">
          <button class="btn btn-dark border-cust rounded-pill px-4 my-2" type="button"><i class="fas fa-eye"></i> PREVIEW</button>
        </a>
      </div>

     <div class="container-fluid px-5">
      <div class="row px-5">
        <div class="col">
          <div class="row p-5 bg-white rounded">
        <div class="col">
          <table id="example" class="display responsive nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>Header Photo</th>
                    <th>Photo</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
              @foreach ($users as $user)
                <tr>
                    <td><img src="{{ url('images/'.$user->aboutus_header) }}" alt="" width="200px"></td>
                    <td><img src="{{ url('images/'.$user->aboutus_photo) }}" alt="" width="200px"></td>
                    <td>{{ $user->aboutus_desc }}</td>
                    <td>
                      <a href="/eAbout/{{ $user->id }}" class="text-decoration-none text-dark" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="fas fa-edit fa-lg m-2"></i></a>
                    </td>
                </tr>           
              @endforeach
            </tbody>
        </table>
        </div>        
      </div>
        </div>        
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form action="/eAbout/<?php echo $users[0]->id; ?>" method="post" style="background-color: white;">
          <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">  
            <div class="form-floating mb-3">
              <input type="file" class="form-control" id="floatingPassword" name="aboutus_header" value="<?php echo $users[0]->aboutus_header; ?>" required>
              <label for="floatingInput">Header Photo</label>
            </div>
            <div class="form-floating mb-3">
              <input type="file" class="form-control" id="floatingPassword" name="aboutus_photo" value="<?php echo $users[0]->aboutus_photo; ?>" required>
              <label for="floatingInput" >Photo</label>
            </div>
            <div class="form-floating">
              <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea" rows="3"  name="aboutus_desc" required><?php echo $users[0]->aboutus_desc; ?></textarea>
              <label for="floatingTextarea">Description</label>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" class="btn bg-1 text-white">Save changes</button>
            </div>          
            </form>
          </div>
        </div>
      </div>
    </div>


    <!-- Body -->

    <!-- Footer -->
    <footer class="bg-1 text-center text-white">
      <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2); margin-top: 18%;">
        © 2022 Copyright:
        <a class="text-white" href="https://mdbootstrap.com/">Katrina G. Urbano</a>
      </div>
    </footer>
    <!-- Footer -->

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!--  Datatable JS -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>    
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script>
    <script>
    $(document).ready( function () {
      var table = $('#example').DataTable({
        columnDefs:[{targets:[0,1,2],className:"truncate"}],
        createdRow: function(row){
           $(row).find(".truncate").each(function(){
              $(this).attr("title", this.innerText);
           });
       }
      });

       $('#exampleModal').modal({ backdrop: 'static', keyboard: false });
    } );


   
    </script>
  </body>
</html>