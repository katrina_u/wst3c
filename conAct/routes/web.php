<?php

use App\Http\Controllers\order_controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [order_controller::class, 'index']);

Route::get('/customer/{cusID}/{name}/{addr}', [order_controller::class, 'customer'])->name('customer');

Route::get('/item/{itemNo}/{name}/{price}', [order_controller::class, 'item'])->name('item');

Route::get('/order/{cusID}/{name}/{orderNo}/{date}', [order_controller::class, 'order'])->name('order');

Route::get('/orderDetails/{transNo}/{orderNo}/{itemID}/{name}/{price}/{qty}', [order_controller::class, 'orderDetails'])->name('orderDetails');