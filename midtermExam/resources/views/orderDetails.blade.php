<!DOCTYPE html>
<html lang="en">
<head>
  <title>Quiz 2</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  
</head>
<body>
	<div class="mt-5">
		<form action="" method="post">	
			<div class="text-center social-btn">
				<div class="container ">
                    <div class="row">
                        <div class="col">
                            <p>Name: <strong>Katrina G. Urbano</strong></p>
                        </div>
                        <div class="col">
                            <p>Year & Section: <strong>3rd Year - IT3C</strong></p>
                        </div>
                    </div>
                </div>
			</div>			
			
		</form>
	</div>
	<div class="container" style="margin-top:30px">
	  <div class="row">
		<div class="col text-center">
			<table class="table table-hover table-bordered">
				<tbody>
					<tr>
						<td class="bg-success text-dark" colspan="2">You're in ORDER DETAILS</td>
					</tr>
					<tr>
						<td class="bg-success text-white" colspan="2"><a href="/">ITEM</a></t
					</tr>
					<tr>
						<td class="bg-success text-white" colspan="2"><a href="/customer">CUSTOMER</a></td>
					<tr>
					<tr>
						<td class="bg-success text-white" colspan="2"><a href="/order_details">ORDER </a></td>
					</tr>
				</tbody>
			</table>	  
		</div>
	  </div>
	</div>      
</body>
</html>

