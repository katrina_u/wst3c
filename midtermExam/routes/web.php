<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/item/{itemNo}/{name}/{price}', function ($itemNo, $name, $price) {
    return  "Name: Katrina Urbano <br> Section: BSIT-3C <br><br>Item number : ". $itemNo ."<br> Item Name : ".$name."<br> Item Price : ".$price;
});

Route::get('/customer/{cusID}/{name}/{age?}', function ($cusID, $name, $age=null) {
    return  "Name: Katrina Urbano <br> Section: BSIT-3C <br><br>Customer ID : ". $cusID ."<br> Customer's Name : ".$name ."<br> Customer's Age : ".$age;
});

Route::get('/order/{cusID}/{name}/{orderNo}/{date}', function ($cusID, $name, $orderNo, $date) {
    return "Name: Katrina Urbano <br> Section: BSIT-3C <br><br>Customer ID : ". $cusID ."<br> Customer's Name : ".$name . "<br> Order No : ".$orderNo . "<br> Date : ".$date;
});

Route::get('/orderDetails/{transNo}/{orderNo}/{itemID}/{name}/{price}/{qty}/{recNo?}', function ($transNo, $orderNo, $itemID, $name, $price, $qty, $recNo = null) {
    return "Name: Katrina Urbano <br> Section: BSIT-3C <br><br>Transaction No : ". $transNo ."<br> Order No : ".$orderNo . "<br> Item ID : ".$itemID . "<br> Name : ". $name . "<br> Price : ". $price . "<br> Quantity : ".$qty. "<br> Receipt No : ".$recNo ."<br> Total : " . $price*$qty;
});
