<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\SetAppointment;
use App\Http\Controllers\ViewController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [CustomAuthController::class, 'login']);
Route::get('/reg', [CustomAuthController::class, 'registration']);
Route::post('/register-user', [CustomAuthController::class, 'registerUser'])->name('register-user');
Route::post('/login-user', [CustomAuthController::class, 'loginUser'])->name('login-user');
//Admin Dashboard
Route::get('/homepage', [CustomAuthController::class, 'Homepage'])->middleware('isLoggedIn');
Route::get('/logout', [CustomAuthController::class, 'logout']);

Route::get('/insert', [SetAppointment::class, 'insertform']);
Route::post('/setapp',[SetAppointment::class, 'insert_appointment']);
Route::get('/appset',[ViewController::class, 'displayAppointment']);

Route::get('/adminHome', [ViewController::class, 'adminDisplayAppointment']);
Route::get('/homepage', [ViewController::class, 'book_appoint']);


