<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>

    <title>Appointment Details</title>

    <style>
      .bg-1 { 
        background-color: #61B0B7;;
      }

      .bg-pic {
         background: radial-gradient(rgba(
              0, 0, 0, 0.25), rgba(0, 0, 0, 0.50)), url('/images/flavor_4.jpg');
        background-size: cover;
        background-position: center;
        height: 60vh;
      }
    </style>

   

  </head>
  <body>
    <!-- NavBar -->
  <nav class="navbar navbar-expand-sm bg-1 justify-content-center">
      <a class="navbar-brand text-white" href="homepage">Appointment Details</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-dark" href="logout">Log out</a>
        </li>
      </ul>
  </nav>
    <!-- NavBar -->

    <!-- Body -->

     <div class="container-fluid p-5">
      <div class="row px-5">
        <p class="navbar-brand text-dark mb-3" href="#">Hello, Doc!</p>
        <div class="col">
          <hr>
            <label for="floatingInput" class="mb-3 mt-3"><h3>APPOINTMENT DETAILS for Approval</h3></label>
          <hr class="mb-5">
            <table id="example" class="mt-2 table table-striped text-center">
            <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">FullName</th>
                  <th scope="col">Age</th>
                  <th scope="col">Phone #</th>
                  <th scope="col">Address</th>
                  <th scope="col">Appointment Date</th>
                  <th scope="col">Appointment Time</th>
                  <th scope="col">Concern</th>
                  <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                  <th scope="row">{{ $user->id }}</th>
                  <td>{{ $user->fname }} </td>
                  <td>{{ $user->age }}</td>
                  <td>{{ $user->pnum }}</td>
                  <td>{{ $user->addr }}</td>
                  <td>{{ $user->appdate }}</td>
                  <td>{{ $user->apptime }}</td>
                  <td>{{ $user->concern }}</td>
                  <td>
                  <a href=""><i class="fas fa-check" style="color: green; height: 30px; font-size:24px;"></i></a>
                  <br>
                  <a href=""><i class="fas fa-times" style="color: red; font-size:24px;"></i>
                  </td></a>
                </tr>
                @endforeach
                
            </tbody>
        </table>
        </div>        
      </div>
    </div>

    <!-- Body -->

    <!-- Footer -->
    <footer class="bg-1 text-center text-white">
      <div class="container p-4">
        <section class="mb-4">
          <!-- Facebook -->
          <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
            ><i class="fab fa-facebook-f"></i
          ></a>

          <!-- Instagram -->
          <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
            ><i class="fab fa-instagram"></i
          ></a>
        </section>
        <section class="mb-4">
          <p>
            Located in Urdaneta City, Pangasinan
          </p>

          <button class="btn btn-danger">
            <a href="/homepage" class="text-white text-decoration-none">Log in as User</a>
          </button>
        </section>
      </div>

      <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2022 De Vera - Hernandez Dental Clinic
      </div>
    </footer>
    <!-- Footer -->

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>

<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        select: true
    } );
} );
</script>