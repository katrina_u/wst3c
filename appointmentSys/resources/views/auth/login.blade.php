<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>Log in</title>

    <style>
      html{
        background: radial-gradient(rgba(
              255, 255, 255, 0.25), rgba(255, 255, 255, 0.50)), url('/images/background_login.png');
        background-size: cover;
      }

      body{
        background-color: inherit;
      }

      .full-width{
        min-height: 100vh;
      }

      .form-sec{
        background-color: white;
        box-shadow: 10px 10px 5px rgba(0, 0, 0, .3);
      }
      @media (max-width: 575.98px){
        form{
          width: 100%;
        }
      }

      .bg-1 { 
        background-color: #61B0B7;
      }

      .ftr-login{
        font-size: 14px;
      }
    </style>

  </head>
  <body>
   <div class="full-width d-flex justify-content-center and align-items-center">
    <form action="{{ route('login-user') }}" method="post">

      @csrf
      <div class="card form-sec rounded-3">
          <h5 class="card-header bg-1 p-4 fs-4 text-center">Welcome!</h5>
        <div class="card-body ms-5 me-5 mt-3">

          <!-- alert -->
          @if(Session::has('success'))
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ Session::get('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
          @endif

          @if(Session::has('fail'))
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ Session::get('fail') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
          @endif
        
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Email</label>
              <input type="email" class="rounded-pill form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ old('email') }}" name="email">
              <span class="text-danger">@error('email') {{$message}} @enderror</span>
            </div>
            <div class="mb-3">
              <label for="exampleInputPassword1" class="form-label">Password</label>
              <input type="password" class="rounded-pill form-control" id="exampleInputPassword1" value="" name="password">
              <span class="text-danger">@error('password') {{$message}} @enderror</span>
            </div>
            <div class="mb-3 text-center mt-3 d-grid gap-2 col-6 mx-auto">            
            <button type="submit" class="fw-bold btn btn-dark rounded-pill bg-1 text-center">Log in</button>
            </div>
        </div>
        <div class="card-footer bg-1 text-center">
          <p class="mt-3 ftr-login m-0 text-white">Don't have an account?</p>
          <a href="/reg" class="text-decoration-none"><p class="text-white" style="font-size: 12px;">Click Here!</p></a>
        </div>
      </div>
    </form>
    </div>
    
    <footer class="bg-1 text-center text-white">
      <div class="container p-4">
        <section class="">
          <p>
            Located in Urdaneta City, Pangasinan
          </p>
        </section>
      </div>

      <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2022 De Vera - Hernandez Dental Clinic
      </div>

    </footer>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>

