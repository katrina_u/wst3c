<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <title>Dental Clinic | Homepage</title>

    <style>
      .bg-1 { 
        background-color: #61B0B7;
      }

      .bg-pic {
         background: radial-gradient(rgba(
              0, 0, 0, 0.25), rgba(0, 0, 0, 0.50)), url('/images/flavor_4.jpg');
        background-size: cover;
        background-position: center;
        height: 60vh;
      }
    </style>

   

  </head>
  <body>
    <!-- NavBar -->
<nav class="navbar navbar-expand-sm bg-1 justify-content-center">
    <a class="navbar-brand text-white" href="homepage">Homepage</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link text-dark" href="appset">Appointment Details</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link text-dark" href="logout">Log out</a>
      </li>
    </ul>
</nav>
    <!-- NavBar -->

    <!-- Body -->

     <div class="container-fluid p-5">
      <div class="row px-5">
        <p class="navbar-brand text-dark mb-3" href="#">Hello! Ready to set an appointment?</p>
        <div class="col">
          <form action="/setapp" class="border border-dark p-4 rounded" method="post">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            <label for="floatingInput" class="mb-3">Appointment</label>
             <div class="input-group mb-3">
              <span class="input-group-text">Firstname</span>
              <input type="text" class="form-control" name="fname">
              <span class="input-group-text">Middle Initial</span>
              <input type="text" class="form-control" name="mname">
              <span class="input-group-text">Lastname</span>
              <input type="text" class="form-control" name="lname">
            </div>
            <div class="input-group mb-3">
              <span class="input-group-text">Age</span>
              <input type="number" class="form-control" name="age">
              <span class="input-group-text">Phone number</span>
              <input type="text" class="form-control" name="pnum">
            </div>
            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingPassword" placeholder="Password" name="addr">
              <label for="floatingInput">Patient's Complete Address</label>
            </div>
            <div class="form-floating mb-3">
              <input type="date" class="form-control" id="floatingPassword" placeholder="Password" name="appdate">
              <label for="floatingInput">Appointment Date</label>
            </div>

            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingPassword" placeholder="Password" name="apptime">
              <label for="floatingInput">Appointment Time</label>
            </div>
            <label class="mb-3">Time Available: 8:00-10:00, 10:00-12:00, 1:00-3:00, 3:00-5:00</label>

            <div class="form-floating">
              <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea" name="concern"></textarea>
              <label for="floatingTextarea">Patient's Concern</label>
            </div>
            <button type="submit" class="btn bg-1 mt-3">Set an Appointment</button>
          </form>
        </div>        
      </div>
    </div>

    <!-- Body -->

    <!-- Footer -->
    <footer class="bg-1 text-center text-white">
      <div class="container p-4">
        <section class="mb-4">
          <!-- Facebook -->
          <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
            ><i class="fab fa-facebook-f"></i
          ></a>

          <!-- Instagram -->
          <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"
            ><i class="fab fa-instagram"></i
          ></a>
        </section>
        <section class="mb-4">
          <p>
            Located in Urdaneta City, Pangasinan
          </p>
          <button class="btn btn-danger">
            <a href="/adminHome" class="text-white text-decoration-none">Log in as Admin</a>
          </button>
        </section>
      </div>

      <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2022 De Vera - Hernandez Dental Clinic
      </div>
    </footer>
    <!-- Footer -->

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  </body>
</html>