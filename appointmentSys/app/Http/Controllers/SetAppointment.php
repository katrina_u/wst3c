<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class SetAppointment extends Controller
{

    public function insertform() {
        return view('homepage');
   }

    public function insert_appointment(Request $request){
        $fname = $request->input('fname');
        $mname = $request->input('mname');
        $lname = $request->input('lname');
        $age = $request->input('age');
        $pnum = $request->input('pnum');
        $addr = $request->input('addr');
        $appdate = $request->input('appdate');
        $app_time = $request->input('apptime');
        $concern = $request->input('concern');

        DB::insert('insert into appointments (fname, mname, lname, age, pnum, addr, appdate, apptime, concern) values(?,?,?,?,?,?,?,?,?)',
            [$fname, $mname, $lname, $age, $pnum, $addr, $appdate, $app_time, $concern]);
        return view('homepage');
    }


    /*
    public function insert_appointment(Request $request){
        $fname = $request->input('fname');
        $mname = $request->input('mname');
        $lname = $request->input('lname');
        $age = $request->input('age');
        $pnum = $request->input('pnum');
        $addr = $request->input('addr');
        $app_date = $request->input('app-date');
        $concern = $request->input('concern');
        DB::insert('insert into appointment (fname, mname, lname, age, pnum, addr, app-date, concern) values(????????)',
            [$fname, $mname, $lname, $age, $pnum, $addr, $app_date, $concern]);
        echo "Record inserted successfully.<br/>";
        echo '<a href = "/insert">Click Here</a> to go back.';
    }
    */
}
