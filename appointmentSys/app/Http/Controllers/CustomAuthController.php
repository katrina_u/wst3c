<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Session;

class CustomAuthController extends Controller
{
    public function login(){
        return view("auth.login");
    }

    public function registration(){
       
        return view("auth.registration");
    }

    public function registerUser(Request $request)
    {
        $request->validate([
            'fname'=>'required',
            'lname'=>'required',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:5|max:12'
        ]);

        $user = new User();
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $res = $user->save();

        if($res){
            return back()->with('success', 'Registered successfully');
        } else{
            return back()->with('fail', 'Something went wrong');
        }
    }

    public function loginUser(Request $request)
    {
        $request->validate([
            'email'=>'required|email',
            'password'=>'required|min:5|max:12'
        ]);

        $user = User::where('email', '=', $request->email)->first();  
        if($user)
        {
            if(Hash::check($request->password, $user->password)){
                $request->session()->put('loginID', $user->id);
                return redirect('homepage');
            } else {
                return back()->with('fail', 'Password do not match');
            }

        } else{ 
            return back()->with('fail', 'This email is not registered.');
        }
    } 

    public function Homepage()
    {
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }
        
        return view('homepage', compact('data'));
    }

    public function adminHome()
    {
        $data = array();
        if (Session::has('loginID')){
            $data = User::where('id', '=', Session::get('loginID'))->first();
        }
        
        return view('admin_homepage', compact('data'));
    }

    public function logout(){
        if(Session::has('loginID')){
            Session::pull('loginID');
            return redirect('/');
        }
    }
}
