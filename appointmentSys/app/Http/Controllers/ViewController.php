<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class ViewController extends Controller
{
    public function book_appoint(){
        $time = DB::select('select * from time_of_appointments');
        return view('homepage',['appoint'=>$time]);
    }

    public function displayAppointment(){
        $users = DB::select('select * from appointments');
        return view('appset',['users'=>$users]);
    }

    public function adminDisplayAppointment(){
        $users = DB::select('select * from appointments');
        return view('admin_homepage',['users'=>$users]);
    }
}
